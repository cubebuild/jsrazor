﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Web.Mvc
{
    public static class JSTemplateExtensions
    {
        public static MvcHtmlString IncludeJSTemplates(this HtmlHelper helper)
        {
            var baseLink = @"<script type=""text/javascript"" src=""{0}""></script>";
            var controller = helper.ViewContext.RouteData.Values["controller"];
            var action = helper.ViewContext.RouteData.Values["action"];

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var url = urlHelper.RouteUrl(new { viewcontroller = controller, viewaction = action, controller = "RES", action = "JSTemplates" });

            return new MvcHtmlString(String.Format(baseLink, url));
        }

    }
}