﻿var TryIt = (function (ns) {
    ns = ns || {};

    ns.Transform = function () {
        var templateElement = $("#template");
        var template = $("<div/>").text(templateElement.val()).html();

        $.post("/res/jsrender", { template: template }, function (data) {
            var errorLog = $("#errorLog");
            errorLog.removeClass("error").html("No errors.");
            try {
                // Syntax highlight the JS
                var source = $("#javascriptSource").empty();
                var code = $("<pre/>");
                source.append(code);
                code.text(data);
                code.snippet("javascript", { style: "acid"});

                var modelJson = $("#model").val();
                eval("var model = " + modelJson);
                eval(data);
                var template = new TryItTemplate();
                $("#renderedResult").empty();
                template.render($("#renderedResult")[0], model);
            } catch (err) {
                errorLog.empty().addClass("error").text(err.toString());
            }
        });
    }
    return ns;
})(TryIt);

$(function () {
    TryIt.Transform();
});