﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CubeBuild.JSRazor.TryIt.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class RESController : Controller
    {
        [HttpGet]
        public ActionResult JSTemplates(string viewcontroller, string viewaction)
        {
            return this.JSTemplate(viewcontroller, viewaction, dialect:"dom");
        }

        [HttpPost]
        public ActionResult JSRender(string template)
        {
            try
            {
                var decodedTemplate = HttpUtility.HtmlDecode(template);
                var engine = new CubeBuild.JSRazor.RazorToJSRenderingEngine();
                var result = engine.Transform(decodedTemplate, "TryItTemplate", new CubeBuild.JSRazor.Dialects.DOMDialect());
                return Content(result, "text/javascript");
            }
            catch (Exception e)
            {
                return Content(e.Message, "text/text");
            }
        }
    }
}
