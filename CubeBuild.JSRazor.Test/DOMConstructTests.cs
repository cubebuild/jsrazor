﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/GPL-3.0
*/

using System;
using CubeBuild.JSRazor.Dialects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeBuild.JSRazor.Test
{
    [TestClass]
    public class DOMConstructTests
    {

        [TestMethod]
        public void DOM_Variable()
        {
            string t = "@t";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"((t!==null&&typeof(t)!=='undefined')?(t):"""").toString().replace("));
        }

        [TestMethod]
        public void DOM_Whitespace()
        {
            string t = "<span>    xyz    </span>    <p></p>     ";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"    xyz    "));
        }

        [TestMethod]
        public void DOM_VariableTrailingDot()
        {
            string t = "@t.";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"((t!==null&&typeof(t)!=='undefined')?(t):"""").toString().replace("));
            Assert.IsTrue(o.Contains(@"createTextNode(""."")"));
        }

        [TestMethod]
        public void DOM_IfElse()
        {
            string t = "@if (true) { true } else { false }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("if (true) {\n true \n} else {\n false \n}"));
        }

        [TestMethod]
        public void DOM_If()
        {
            string t = "@if (true) { true }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("if (true) {\n true \n}"));
            Assert.IsFalse(o.Contains("if (true) {\n true \n} else {\n false \n}"));
        }

        [TestMethod]
        public void DOM_HtmlRaw()
        {
            string t = @"@Html.Raw(""<a/>"")";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"Target.insertAdjacentHTML('beforeend', (""<a/>"").toString());"));
        }

        [TestMethod]
        public void DOM_CallHelper()
        {
            string t = @"@f(x)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"f(Target,x);"));
        }

        [TestMethod]
        public void DOM_HelperX2()
        {
            string t = @"@helper x(a,b){ pass; }@helper y(a,b,c){ <a/> }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore("this.x = function(Target, a,b) {  pass; }"));
            Assert.IsTrue(o.ContainsIgnore(@"this.y = function(Target, a,b,c) { var _1 = document.createElement(""a""); Target.appendChild(_1); this.Prev = _1; var _2 = document.createTextNode("" ""); Target.appendChild(_2); }; "));
            Assert.IsTrue(o.ContainsIgnore(@"this.render = function(Target, Model) {   }"));
        }

        [TestMethod]
        public void DOM_RunawayQuote()
        {
            string t = @"@helper x(){ "" }";
            var r = new RazorToJSRenderingEngine();
            try
            {
                var o = r.Transform(t, "x", new DOMDialect());
                Assert.Fail();
            }
            catch { }

        }

        [TestMethod]
        public void DOM_Helper()
        {
            string t = @"@helper f(x){true}";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"this.f = function(Target, x) {"));
            Assert.IsTrue(o.Contains(@"this.render = function(Target, Model) {"));
        }

        [TestMethod]
        public void DOM_Helper2()
        {
            string t = @"@helper f(){true}";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"this.f = function(Target) {"));
            Assert.IsTrue(o.Contains(@"this.render = function(Target, Model) {"));
        }

        [TestMethod]
        public void DOM_Function()
        {
            string t = @"@function f(x){true};";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore("this.f = function(x) { true }"));
            Assert.IsTrue(o.Contains(@"this.render = function(Target, Model) {"));
        }

        [TestMethod]
        public void DOM_ForEach()
        {
            string t = @"@foreach (var x in y) { }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"for (var _1 = 0; _1 < y.length; _1++) { var x = y[_1];"));
        }

        [TestMethod]
        public void DOM_For()
        {
            string t = @"@for (X) { }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore("for (X) { }"));
        }

        [TestMethod]
        public void DOM_DelimitedVariable()
        {
            string t = @"@(n)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"Target.insertAdjacentHTML('beforeend', ((n!==null&&typeof(n)!=='undefined')?(n):"""").toString().replace("));
        }

        [TestMethod]
        public void DOM_Comment()
        {
            string t = @"@*x*@";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"/*x*/"));
        }

        [TestMethod]
        public void DOM_CodeBlock()
        {
            string t = @"@{/*x*/}";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains(@"/*x*/"));
        }

        [TestMethod]
        public void DOM_BaseTemplate()
        {
            string t = @"";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore("function x() { this.render = function(Target, Model) {  }; this.$Dialect = \"dom\"; }"));
        }

        [TestMethod]
        public void DOM_ContentRecurse()
        {
            var t = "@helper x(y) { if (y) { @(y+1) } }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("if (y)"));
            Assert.IsTrue(o.Contains("Target.insertAdjacentHTML('beforeend', ((y+1!==null&&typeof(y+1)!=='undefined')?(y+1):\"\").toString().replace(/&/g,\"&amp;\").replace(/</g, \"&lt;\").replace(/>/g, \"&gt;\"));"));
        }

        [TestMethod]
        public void DOM_CompoundContent()
        {
            var t = "<a>\n</a>\n";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"var _1 = document.createElement(""a"");Target.appendChild(_1);var $Context = this.Prev = _1; (function(Target){}).call(this, $Context); this.Prev = $Context;}"));
        }

        [TestMethod]
        public void DOM_RenderHelper()
        {
            var t = "@render Controls.TextBox(Model)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("new Controls().TextBox(Target, Model);"));
        }

        [TestMethod]
        public void DOM_Render()
        {
            var t = "@render Other(Model)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("new Other().render(Target, Model);"));
        }

        [TestMethod]
        public void DOM_Tags()
        {
            var t = "<a b='c'><b c='d'/><c /><d></d></a>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"document.createElement(""a"");"));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""b"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ""c"""));
            Assert.IsTrue(o.ContainsIgnore(@"document.createElement(""b"");"));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""c"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ""d"""));
            Assert.IsTrue(o.ContainsIgnore(@"var _3 = document.createElement(""c""); Target.appendChild(_3);"));
            Assert.IsTrue(o.ContainsIgnore(@"var _4 = document.createElement(""d""); Target.appendChild(_4); var $Context = this.Prev = _4; (function(Target){"));
        }

        [TestMethod]
        public void DOM_Attributes()
        {
            var t = "<input name=\"@var\"/>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"document.createElement(""input"");"));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""name"""));
            Assert.IsTrue(o.ContainsIgnore("rslt += ((var)?(var):\"\").toString().replace(/&/g,\"&amp;\").replace(/</g, \"&lt;\").replace(/>/g, \"&gt;\");"));
        }

        [TestMethod]
        public void DOM_Attributes2()
        {
            var t = "<input name=\"@(var?1:2)\"/>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"document.createElement(""input"");"));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""name"""));
            Assert.IsTrue(o.ContainsIgnore("rslt += ((var?1:2)?(var?1:2):\"\").toString().replace(/&/g,\"&amp;\").replace(/</g, \"&lt;\").replace(/>/g, \"&gt;\");"));
        }

        [TestMethod]
        public void DOM_Attributes3()
        {
            var t = "<in cl='a' ef=\"b\" en='@(b)' ex = \"@r\"/>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"document.createElement(""in"");"));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""cl"""));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""ef"""));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""en"""));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""ex"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ""a"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ""b"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ((b)?(b):"""")"));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ((r)?(r):"""")"));
        }

        [TestMethod]
        public void DOM_Attributes4()
        {
            var t = "<in cl='a' ef=\"b\" en='@(b)' ex = \"@r\"></in>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"document.createElement(""in"");"));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""cl"""));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""ef"""));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""en"""));
            Assert.IsTrue(o.ContainsIgnore(@"setAttribute(""ex"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ""a"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ""b"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ((b)?(b):"""")"));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ((r)?(r):"""")"));
        }

        [TestMethod]
        public void DOM_BindEvent()
        {
            var t = "<button>@event click(function(){})</button>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"Target.addEventListener('click', function(){}, false);"));
        }

        [TestMethod]
        public void DOM_AttachEvent()
        {
            var t = "<button>@attach click(function(){})</button>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"Prev.addEventListener('click', function(){}, false);"));
        }

        [TestMethod]
        public void DOM_AttributeSpanned()
        {
            var t = "<button id='a' \n name='b'></button>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"""a"""));
            Assert.IsTrue(o.ContainsIgnore(@"""b"""));
        }

        [TestMethod]
        public void DOM_AttributeEmpty()
        {
            var t = @"<button id=""""></button>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"""id"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt = """""));
        }

        [TestMethod]
        public void DOM_AttributeEmpty2()
        {
            var t = @"<img src=""x"" id="""" />";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"""id"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt = """""));
            Assert.IsTrue(o.ContainsIgnore(@"""src"""));
            Assert.IsTrue(o.ContainsIgnore(@"rslt += ""x"""));
        }

        [TestMethod]
        public void DOM_Contained()
        {
            var t = "@if (true) { <img src=\"/a/b/c.png\" alt=\"Warning\"/> }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.ContainsIgnore(@"""src"""));
            Assert.IsTrue(o.ContainsIgnore(@"""/a/b/c.png"""));
        }

        [TestMethod]
        public void DOM_AtAt()
        {
            var t = "@if(true){ @@ }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsFalse(o.ContainsIgnore(@"@@"));
            Assert.IsTrue(o.ContainsIgnore(@"@"));
        }

        [TestMethod]
        public void DOM_Set()
        {
            var t = "<option/>@set selected(Model)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("this.Prev.setAttribute('selected', Model);"));
        }

        [TestMethod]
        public void DOM_Regex()
        {
            var t = "@if (True) { var regex = /^([+-]{0,1})(.*?)@(.*)$/g; }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("{0,1}"));
        }

        [TestMethod]
        public void DOM_Null()
        {
            var t = "@null";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("(null!==null&&typeof(null)!=='undefined')"));
        }

        [TestMethod]
        public void DOM_NullModel()
        {
            var t = "@Model";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("(Model!==null&&typeof(Model)!=='undefined')"));
        }

        [TestMethod]
        public void DOM_Containment()
        {
            var t = "a<span>b</span>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x", new DOMDialect());
            Assert.IsTrue(o.Contains("var _1 = document.createTextNode(\"a\");"));
        }

    }
}
