﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CubeBuild.JSRazor.Test
{
    public static class StringExtensions
    {
        public static bool ContainsIgnore(this string source, string compare)
        {
            
            var a = Regex.Replace(source.Replace(" ", "").Replace("\n", "").Replace("\t", ""), "_[0-9a-z]{32}", "_0", RegexOptions.Singleline);
            var b = Regex.Replace(compare.Replace(" ", "").Replace("\n", "").Replace("\t", ""), "_[0-9a-z]{32}", "_0", RegexOptions.Singleline);

            return a.Contains(b);
        }
    }
}
