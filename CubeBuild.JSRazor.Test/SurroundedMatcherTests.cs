﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/GPL-3.0
*/

using System;
using CubeBuild.JSRazor.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeBuild.JSRazor.Test
{
    [TestClass]
    public class SurroundedMatcherTests
    {
        [TestMethod]
        public void SurroundedPositiveSingle()
        {
            string t = "(a)";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 3);
            Assert.AreEqual(matcher.Values(t)[1], "a");
        }

        [TestMethod]
        public void SurroundedPositive()
        {
            string t = "(a)x";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 3);
            Assert.AreEqual(matcher.Values(t)[1], "a");
        }

        [TestMethod]
        public void SurroundedNegativeSingle()
        {
            string t = "n(a)";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "a");
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void SurroundedNegative()
        {
            string t = "{n}";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "n");
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void SurroundedPositiveSingleChain()
        {
            string t = "(n)(a)";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')').Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 6);
            Assert.AreEqual(matcher.Values(t)[1], "n");
            Assert.AreEqual(matcher.Values(t)[2], "a");
        }

        [TestMethod]
        public void SurroundedPositiveChain()
        {
            string t = "(n)(a)s";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')').Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 6);
            Assert.AreEqual(matcher.Values(t)[1], "n");
            Assert.AreEqual(matcher.Values(t)[2], "a");
        }

        [TestMethod]
        public void SurroundedNegativeSingleChain()
        {
            string t = "(n)[a]s";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')').Surrounded('(', ')'));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 3);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "n");
                Assert.AreEqual(matcher.Values(t)[2], "a");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void SurroundedNegativeChain()
        {
            string t = "(n)[a]sty";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')').Surrounded('(', ')'));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 3);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "n");
                Assert.AreEqual(matcher.Values(t)[2], "a");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void SurroundedNull()
        {
            string t = null;
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch { }

        }

        [TestMethod]
        public void SurroundedEmpty()
        {
            string t = "";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void SurroundedCompoundSingle()
        {
            string t = "((a)(b))";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 8);
            Assert.AreEqual(matcher.Values(t)[1], "(a)(b)");
        }

        [TestMethod]
        public void SurroundedCompound()
        {
            string t = "((a)(b)){d}";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 8);
            Assert.AreEqual(matcher.Values(t)[1], "(a)(b)");
        }

        [TestMethod]
        public void SurroundedUnbalanced()
        {
            // this should fail, not pass, because the bracketing is not well formed, even tho our opening bracket is matched
            string t = "((a)(b){d}";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            try
            {
                Assert.IsFalse(matcher.IsMatch(t, ref l));
                Assert.Fail();
            }
            catch { }
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "(a)(b");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void SurroundedUnbalancedString()
        {
            // this should pass as internal brakets are within javascript strings
            string t = "(if\"(a\"==')b)'){d}";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 15);
            Assert.AreEqual(matcher.Values(t)[1], "if\"(a\"==')b)'");
        }

        [TestMethod]
        public void SurroundedUnbalancedStringCompoundSingleInside()
        {
            // this should pass as internal brakets are within javascript strings
            string t = "(if\"(a ==')b)\"){d}";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 15);
            Assert.AreEqual(matcher.Values(t)[1], "if\"(a ==')b)\"");
        }

        [TestMethod]
        public void SurroundedUnbalancedStringCompoundDoubleInside()
        {
            // this should pass as internal brakets are within javascript strings
            string t = "(if'(a ==\")b)'){d}";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 15);
            Assert.AreEqual(matcher.Values(t)[1], "if'(a ==\")b)'");
        }

        [TestMethod]
        public void SurroundedUnbalancedStringCompoundBothInside()
        {
            // this should pass as internal brakets are within javascript strings
            string t = "(if'(a \"==\")b)'){d}";
            var matcher = Matcher.Chain(m => m.Surrounded('(', ')'));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 16);
            Assert.AreEqual(matcher.Values(t)[1], "if'(a \"==\")b)'");
        }

    }
}
