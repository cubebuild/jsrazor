﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeBuild.JSRazor.Test
{
    [TestClass]
    public class CommandTests
    {
        public string CWD {get;set;}
        public string CMD {get;set;}
        public string CSD {get;set;}

        [TestInitialize]
        public void SetupCommand()
        {
            var rdir = System.IO.Directory.GetCurrentDirectory();
            CWD = System.IO.Path.Combine(rdir, @"..\..");
            CMD = System.IO.Path.Combine(CWD, @"..\CubeBuild.JSRazor.Command\bin\debug\CubeBuild.JSRazor.Command.exe");
            CSD = System.IO.Path.Combine(CWD, "CommandTest");
        }

        [TestMethod]
        public void WorkingDirectory()
        {
            var empty = System.IO.Path.Combine(CSD, "A\\");
            var processInfo = new System.Diagnostics.ProcessStartInfo()
            {
                FileName = CMD,
                WorkingDirectory = empty,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            var process = System.Diagnostics.Process.Start(processInfo);
            process.WaitForExit();

            using (var reader = process.StandardOutput)
            {
                var result = reader.ReadToEnd();
                Assert.IsTrue(result.Contains("Script A"));
                Assert.IsFalse(result.Contains("Script B"));
                Assert.IsTrue(result.Contains("Template A"));
                Assert.IsFalse(result.Contains("Template B"));
            }
        }

        [TestMethod]
        public void ExplicitDirectory1()
        {
            var a = System.IO.Path.Combine(CSD, "A\\");
            var processInfo = new System.Diagnostics.ProcessStartInfo()
            {
                FileName = CMD,
                Arguments = a,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            var process = System.Diagnostics.Process.Start(processInfo);
            process.WaitForExit();

            using (var reader = process.StandardOutput)
            {
                var result = reader.ReadToEnd();
                Assert.IsTrue(result.Contains("Script A"));
                Assert.IsFalse(result.Contains("Script B"));
                Assert.IsTrue(result.Contains("Template A"));
                Assert.IsFalse(result.Contains("Template B"));
            }
        }

        [TestMethod]
        public void ExplicitDirectory2()
        {
            var a = System.IO.Path.Combine(CSD, "A\\");
            var b = System.IO.Path.Combine(CSD, "B\\");
            var processInfo = new System.Diagnostics.ProcessStartInfo()
            {
                FileName = CMD,
                Arguments = String.Join(" ", a, b),
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            var process = System.Diagnostics.Process.Start(processInfo);
            process.WaitForExit();

            using (var reader = process.StandardOutput)
            {
                var result = reader.ReadToEnd();
                Assert.IsTrue(result.Contains("Script A"));
                Assert.IsTrue(result.Contains("Script B"));
                Assert.IsTrue(result.Contains("Template A"));
                Assert.IsTrue(result.Contains("Template B"));
            }
        }

        [TestMethod]
        public void ExplicitFiles1()
        {
            var a = System.IO.Path.Combine(CSD, "A\\script.js");
            var b = System.IO.Path.Combine(CSD, "A\\template.jshtml");
            var processInfo = new System.Diagnostics.ProcessStartInfo() { 
                FileName = CMD,
                Arguments = String.Join(" ", a, b),
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            var process = System.Diagnostics.Process.Start(processInfo);
            process.WaitForExit();

            using(var reader = process.StandardOutput){
                var result = reader.ReadToEnd();
                Assert.IsTrue(result.Contains("Script A"));
                Assert.IsFalse(result.Contains("Script B"));
                Assert.IsTrue(result.Contains("Template A"));
                Assert.IsFalse(result.Contains("Template B"));

            }
        }

        [TestMethod]
        public void ExplicitFiles2()
        {
            var a = System.IO.Path.Combine(CSD, "A\\script.js");
            var b = System.IO.Path.Combine(CSD, "B\\script.js");
            var processInfo = new System.Diagnostics.ProcessStartInfo()
            {
                FileName = CMD,
                Arguments = String.Join(" ", a, b),
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            var process = System.Diagnostics.Process.Start(processInfo);
            process.WaitForExit();

            using (var reader = process.StandardOutput)
            {
                var result = reader.ReadToEnd();
                Assert.IsTrue(result.Contains("Script A"));
                Assert.IsTrue(result.Contains("Script B"));
                Assert.IsFalse(result.Contains("Template A"));
                Assert.IsFalse(result.Contains("Template B"));
            }
        }

        [TestMethod]
        public void ExplicitMix()
        {
            var a = System.IO.Path.Combine(CSD, "A\\");
            var b = System.IO.Path.Combine(CSD, "B\\script.js");
            var processInfo = new System.Diagnostics.ProcessStartInfo()
            {
                FileName = CMD,
                Arguments = String.Join(" ", a, b),
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            var process = System.Diagnostics.Process.Start(processInfo);
            process.WaitForExit();

            using (var reader = process.StandardOutput)
            {
                var result = reader.ReadToEnd();
                Assert.IsTrue(result.Contains("Script A"));
                Assert.IsTrue(result.Contains("Script B"));
                Assert.IsTrue(result.Contains("Template A"));
                Assert.IsFalse(result.Contains("Template B"));
            }
        }
    }
}
