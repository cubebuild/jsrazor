﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/GPL-3.0
*/

using System;
using CubeBuild.JSRazor.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeBuild.JSRazor.Test
{
    [TestClass]
    public class LiteralMatcherTests
    {
        [TestMethod]
        public void LiteralPositiveSingle()
        {
            string t = "@";
            var matcher = Matcher.Chain(m => m.Literal("@"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            Assert.AreEqual(matcher.Values(t)[1], "@");
        }

        [TestMethod]
        public void LiteralPositive()
        {
            string t = "@more";
            var matcher = Matcher.Chain(m => m.Literal("@"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            Assert.AreEqual(matcher.Values(t)[1], "@");
        }

        [TestMethod]
        public void LiteralNegativeSingle()
        {
            string t = "n";
            var matcher = Matcher.Chain(m => m.Literal("@"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch
            {
            }
            
        }

        [TestMethod]
        public void LiteralNegative()
        {
            string t = "neg";
            var matcher = Matcher.Chain(m => m.Literal("@"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void LiteralPositiveSingleChain()
        {
            string t = "@chain";
            var matcher = Matcher.Chain(m => m.Literal("@").Literal("chain"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 6);
            Assert.AreEqual(matcher.Values(t)[1], "@");
            Assert.AreEqual(matcher.Values(t)[2], "chain");
        }

        [TestMethod]
        public void LiteralPositiveChain()
        {
            string t = "@chainme";
            var matcher = Matcher.Chain(m => m.Literal("@").Literal("chain"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 6);
            Assert.AreEqual(matcher.Values(t)[1], "@");
            Assert.AreEqual(matcher.Values(t)[2], "chain");
        }

        [TestMethod]
        public void LiteralNegativeSingleChain()
        {
            string t = "@train";
            var matcher = Matcher.Chain(m => m.Literal("@").Literal("chain"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.AreEqual(matcher.Values(t)[2], "chain");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void LiteralNegativeChain()
        {
            string t = "@trainme";
            var matcher = Matcher.Chain(m => m.Literal("@").Literal("chain"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.AreEqual(matcher.Values(t)[2], "chain");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void LiteralNull()
        {
            string t = null;
            var matcher = Matcher.Chain(m => m.Literal("@"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void LiteralEmpty()
        {
            string t = "";
            var matcher = Matcher.Chain(m => m.Literal("@"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch { }
        }
    }
}
