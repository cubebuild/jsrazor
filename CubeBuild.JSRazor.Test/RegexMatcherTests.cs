﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/GPL-3.0
*/

using System;
using CubeBuild.JSRazor.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeBuild.JSRazor.Test
{
    [TestClass]
    public class RegexMatcherTests
    {
        [TestMethod]
        public void RegexPositiveSingle()
        {
            string t = "@";
            var matcher = Matcher.Chain(m => m.Regex("[@]"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            Assert.AreEqual(matcher.Values(t)[1], "@");
        }

        [TestMethod]
        public void RegexPositive()
        {
            string t = "@more";
            var matcher = Matcher.Chain(m => m.Regex("[@]"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            Assert.AreEqual(matcher.Values(t)[1], "@");
        }

        [TestMethod]
        public void RegexNegativeSingle()
        {
            string t = "n";
            var matcher = Matcher.Chain(m => m.Regex("[@]"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void RegexNegative()
        {
            string t = "neg";
            var matcher = Matcher.Chain(m => m.Regex("[@]"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void RegexPositiveSingleChain()
        {
            string t = "@chain";
            var matcher = Matcher.Chain(m => m.Regex("[@]").Regex("c...n"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 6);
            Assert.AreEqual(matcher.Values(t)[1], "@");
            Assert.AreEqual(matcher.Values(t)[2], "chain");
        }

        [TestMethod]
        public void RegexPositiveChain()
        {
            string t = "@chainme";
            var matcher = Matcher.Chain(m => m.Regex("[@]").Regex("c...n"));
            int l = 0;
            Assert.IsTrue(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 6);
            Assert.AreEqual(matcher.Values(t)[1], "@");
            Assert.AreEqual(matcher.Values(t)[2], "chain");
        }

        [TestMethod]
        public void RegexNegativeSingleChain()
        {
            string t = "@train";
            var matcher = Matcher.Chain(m => m.Regex("[@]").Regex("c...n"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.AreEqual(matcher.Values(t)[2], "chain");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void RegexNegativeChain()
        {
            string t = "@trainme";
            var matcher = Matcher.Chain(m => m.Regex("[@]").Regex("c...n"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 1);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.AreEqual(matcher.Values(t)[2], "chain");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void RegexNull()
        {
            string t = null;
            var matcher = Matcher.Chain(m => m.Regex("[@]"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch { }
        }

        [TestMethod]
        public void RegexEmpty()
        {
            string t = "";
            var matcher = Matcher.Chain(m => m.Regex("[@]"));
            int l = 0;
            Assert.IsFalse(matcher.IsMatch(t, ref l));
            Assert.AreEqual(l, 0);
            try
            {
                Assert.AreEqual(matcher.Values(t)[1], "@");
                Assert.Fail();
            }
            catch { }
        }
    }
}
