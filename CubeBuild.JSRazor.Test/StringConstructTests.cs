﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/GPL-3.0
*/

using System;
using CubeBuild.JSRazor.Dialects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeBuild.JSRazor.Test
{
    [TestClass]
    public class StringConstructTests
    {

        [TestMethod]
        public void STR_Variable()
        {
            string t = "@t";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains( @"((t)?(t):"""").toString().replace("));
        }

        [TestMethod]
        public void STR_VariableTrailingDot()
        {
            string t = "@t.";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"((t)?(t):"""").toString().replace("));
            Assert.IsTrue(o.Contains(@"rslt += ""."""));
        }

        [TestMethod]
        public void STR_IfElse()
        {
            string t = "@if (true) { true } else { false }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("if (true) {\n true \n} else {\n false \n}"));
        }

        [TestMethod]
        public void STR_If()
        {
            string t = "@if (true) { true }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("if (true) {\n true \n}"));
            Assert.IsFalse(o.Contains("if (true) {\n true \n} else {\n false \n}"));
        }

        [TestMethod]
        public void STR_HtmlRaw()
        {
            string t = @"@Html.Raw(""<a/>"")";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"rslt += ""<a/>"";"));
        }

        [TestMethod]
        public void STR_CallHelper()
        {
            string t = @"@f(x)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"rslt += f(x);"));
        }

        [TestMethod]
        public void STR_HelperX2()
        {
            string t = @"@helper x(a,b){ pass; }@helper y(a,b,c){ <a/> }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.ContainsIgnore("this.x = function(a,b) { rslt = \"\"; pass; return rslt; }"));
            Assert.IsTrue(o.ContainsIgnore(@"this.y = function(a,b,c) { rslt = """";  rslt += ""<a/>""; rslt += "" ""; return rslt; }"));
            Assert.IsTrue(o.ContainsIgnore(@"this.render = function(Model) { rslt = """"; return rslt; }"));
        }

        [TestMethod]
        public void STR_RunawayQuote()
        {
            string t = @"@helper x(){ "" }";
            var r = new RazorToJSRenderingEngine();
            try
            {
                var o = r.Transform(t, "x");
                Assert.Fail();
            }
            catch { }

        }

        [TestMethod]
        public void STR_Helper()
        {
            string t = @"@helper f(x){true}";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"this.f = function(x) {"));
            Assert.IsTrue(o.Contains(@"this.render = function(Model) {"));
        }

        [TestMethod]
        public void STR_Function()
        {
            string t = @"@function f(x){true};";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("this.f = function(x) {\ntrue\n}"));
            Assert.IsTrue(o.Contains(@"this.render = function(Model) {"));
        }

        [TestMethod]
        public void STR_ForEach()
        {
            string t = @"@foreach (var x in y) { }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.ContainsIgnore(@"for (var _1 = 0; _1 < y.length; _1++) { var x = y[_1];"));
        }

        [TestMethod]
        public void STR_For()
        {
            string t = @"@for (X) { }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.ContainsIgnore("for (X) { }"));
        }

        [TestMethod]
        public void STR_DelimitedVariable()
        {
            string t = @"@(n)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"rslt += ((n)?(n):"""").toString().replace("));
        }

        [TestMethod]
        public void STR_Comment()
        {
            string t = @"@*x*@";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"/*x*/"));
        }

        [TestMethod]
        public void STR_CodeBlock()
        {
            string t = @"@{/*x*/}";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"/*x*/"));
        }

        [TestMethod]
        public void STR_BaseTemplate()
        {
            string t = @"";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.AreEqual("function x() {\nthis.render = function(Model) {\nrslt = \"\";\n\nreturn rslt;\n\n};\n\nthis.$Dialect = \"string\";\n}", o);
        }

        [TestMethod]
        public void STR_ContentRecurse()
        {
            var t = "@helper x(y) { if (y) { @(y+1) } }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("if (y)"));
            Assert.IsTrue(o.Contains("rslt += ((y+1)?(y+1):\"\").toString().replace(/&/g,\"&amp;\").replace(/</g, \"&lt;\").replace(/>/g, \"&gt;\");"));
        }

        [TestMethod]
        public void STR_CompoundContent()
        {
            var t = "<a>\n</a>\n";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("rslt += \"<a>\";"));
            Assert.IsTrue(o.Contains("rslt += \"</a>\";"));
        }

        [TestMethod]
        public void STR_RenderHelper()
        {
            var t = "@render Controls.TextBox(Model)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("rslt += new Controls().TextBox(Model);"));
        }

        [TestMethod]
        public void STR_Render()
        {
            var t = "@render Other(Model)";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains("rslt += new Other().render(Model);"));
        }

        [TestMethod]
        public void STR_Tags()
        {
            var t = "<a b='c'><b c='d'/><c /><d></d></a>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.Contains(@"rslt += ""<a "";"));
            Assert.IsTrue(o.Contains(@"rslt += ""b='c'"";"));
            Assert.IsTrue(o.Contains(@"rslt += ""<b "";"));
            Assert.IsTrue(o.Contains(@"rslt += ""c='d'"";"));
            Assert.IsTrue(o.Contains(@"rslt += ""<c/>"";"));
            Assert.IsTrue(o.Contains(@"rslt += ""<d>"";"));
            Assert.IsTrue(o.Contains(@"rslt += ""</d>"";"));
            Assert.IsTrue(o.Contains(@"rslt += ""</a>"";"));
        }

        [TestMethod]
        public void STR_Attributes()
        {
            var t = "<input name=\"@var\"/>";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.ContainsIgnore("rslt += \"<input \";"));
            Assert.IsTrue(o.ContainsIgnore("rslt += \"name=\\\"\";"));
            Assert.IsTrue(o.ContainsIgnore("rslt += ((var)?(var):\"\").toString().replace(/&/g,\"&amp;\").replace(/</g, \"&lt;\").replace(/>/g, \"&gt;\");"));
            Assert.IsTrue(o.ContainsIgnore("rslt += \"\\\"\";"));
        }

        [TestMethod]
        public void STR_Special()
        {
            var t = "&nbsp;";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.ContainsIgnore("rslt += \"&nbsp;\""));
        }

        [TestMethod]
        public void STR_Contained()
        {
            var t = "@if (true) { <img src=\"/a/b/c.png\" alt=\"Warning\"/> }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsTrue(o.ContainsIgnore(@"src=\""/a/b/c.png\"""));
        }

        [TestMethod]
        public void STR_AtAt()
        {
            var t = "@if(true){ @@ }";
            var r = new RazorToJSRenderingEngine();
            var o = r.Transform(t, "x");
            Assert.IsFalse(o.ContainsIgnore(@"@@"));
            Assert.IsTrue(o.ContainsIgnore(@"@"));
        }

    }
}
