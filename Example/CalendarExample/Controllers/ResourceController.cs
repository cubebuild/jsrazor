﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CalendarExample.Controllers
{
    public class ResourceController : Controller
    {
        public ActionResult JSTmpl(string viewController, string viewAction, string dialect = "string")
        {
            return this.JSTemplate(viewController, viewAction, dialect:dialect);
        }

    }
}
