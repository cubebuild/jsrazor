﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CalendarExample.Controllers
{
    public class BindingController : Controller
    {
        //
        // GET: /Calendar/

        public ActionResult Index()
        {
            return View();
        }

        Person[] PersonList =  {
                                  new Person() {
                                      ID = 1,
                                      Name = new Name() { First = "Adrian", Last = "Holland"},
                                      Address = new Address() {
                                          Street = "Jackson Crt",
                                          City = "Strathfieldsaye",
                                          State = "Victoria",
                                          Country = "Australia",
                                          Postcode = "3553"
                                      }
                                  },

                                  new Person(){
                                      ID = 2,
                                      Name = new Name() { First = "Sam", Last = "Taylor"},
                                      Address = new Address() {
                                          Street = "Pines Rd",
                                          City = "Robe",
                                          State = "South Australia",
                                          Country = "Australia",
                                          Postcode = "8343"
                                      }
                                  }, 

                                  new Person() {
                                      ID = 3,
                                      Name = new Name() { First = "Greg", Last = "Jones"},
                                      Address = new Address() {
                                          Street = "Yates Blvd",
                                          City = "Caroline Springs",
                                          State = "Victoria",
                                          Country = "Australia",
                                          Postcode = "3345"
                                      }
                                  }
                               };

        /// <summary>
        /// Return a list of people and their ID's
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            return Json(new { Success = true, Items = PersonList.Select(p => new { p.ID, Name = p.Name }) });
        }

        /// <summary>
        /// Return a single model object
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Object(int id)
        {
            return Json(new { Success = true, Person = PersonList.Where(p => p.ID == id).FirstOrDefault() });
        }

        public class Person
        {
            public int ID { get; set; }
            public Name Name { get; set; }
            public Address Address { get; set; }
        }
        public class Name
        {
            public string First { get; set; }
            public string Last { get; set; }
        }

        public class Address
        {
            public string Street { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Postcode { get; set; }
            public string Country { get; set; }
        }
    }
}
