﻿

function Person(first, last) {
    this.First = CubeBuild.Field(first);
    this.Last = CubeBuild.Field(last);
}
CubeBuild.Observable(Person);

var team = new CubeBuild.ListenArray([
    new Person("Jerry", "Lewis"),
    new Person("Lenny", "Henry")
]);


function changeall() {
    team[0].First = "Tony";
    team[0].changed();
}

$(function () {
    team[0].listen(CubeBuild.Call(function(person) { alert("This message is in response to a notification raised on the first team member, it is then re-rendered automatically using a second template that highlights to row."); }, team[0]));

    $("#team").bindObject(Team, team);
    team.push(new Person("Ben", "Elton"));
    team[0].notify();
});