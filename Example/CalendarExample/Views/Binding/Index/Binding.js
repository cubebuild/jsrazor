﻿// Set up a global to store the value of the ID for currently selected person
var BindingValues = {
    SelectedPerson: CubeBuild.Field(1)
};


$(function () {
    // Respond to a change in the seleted person field, render the person into the #person div
    BindingValues.SelectedPerson.listen(function () {
        $("#person").postTemplate("/binding/object", { id: BindingValues.SelectedPerson.get() }, Person);
    });
    // Get the selector list from the server
    // The synchronizing bind will call notify on the bound field automatically,
    // causing the person to be downloaded from the server
    $("#personSelector").postTemplate("/binding/list", {}, Selector);

    // Change selection to person 2
    BindingValues.SelectedPerson.set(2);
});
