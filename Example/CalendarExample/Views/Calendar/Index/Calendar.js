﻿
$(function () {
    var t = new Calendar();
    $("#calendar").html(t.render({
        Month: 0,
        Year: 2013,
        Events: [
            { Date: "1/1/2013", Event: "School Starts" },
            { Date: "1/3/2013", Event: "Free Day" },
            { Date: "1/7/2013", Event: "Car Serviced" },
            { Date: "1/7/2013", Event: "Dinner with Friends" },
            { Date: "1/12/2013", Event: "Rubbish Collected" },
            { Date: "1/18/2013", Event: "Town Planning Meeting" },
            { Date: "1/23/2013", Event: "School Curriculum Day" }
        ]
    }));
});

Calendar.prototype.DayNumber = function (month, year, weekNumber, dayNumber) {
    /* Return appropriate day number, or nothing for a day that is not valid in the month */
    var actualDayNumber = this.Today(month, year, weekNumber, dayNumber);
    if (actualDayNumber <= 0) { return ""; }
    var availableDays = new Date(year, month + 1, 0).getDate();
    if (actualDayNumber > availableDays) { return ""; }
    return actualDayNumber;
}

Calendar.prototype.Today = function (month, year, weekNumber, dayNumber) {
    /* Find the day number based on the cell references for a given month and year */
    var firstDate = new Date(year, month, 1);
    var dateOffset = firstDate.getDay();
    return (weekNumber * 7) + dayNumber - dateOffset + 1;
}