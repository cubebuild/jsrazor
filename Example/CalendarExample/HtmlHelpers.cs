﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Web.Mvc
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString IncludeJSTemplates(this HtmlHelper helper, bool embedded = true, string dialect = "string")
        {
            var baseLink = @"<script type=""text/javascript"" src=""{0}""></script>";
            var controller = helper.ViewContext.RouteData.Values["controller"];
            var action = helper.ViewContext.RouteData.Values["action"];

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var url = urlHelper.RouteUrl(new { controller="resource", action="jstmpl", viewcontroller = controller, viewaction = action, dialect = dialect});

            return new MvcHtmlString(String.Format(baseLink, url));
        }
    }
}