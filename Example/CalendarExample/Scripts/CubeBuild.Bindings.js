﻿var CubeBuild = (function (ns) {
    ns = ns || {};
    ns.Observable = function (type) {

        type.prototype.listen = function (callback) {
            if (!this.listeners) { this.listeners = []; }
            this.listeners.push(callback);
        }

        type.prototype.stopListening = function (callback) {
            if (!this.listeners) { return; }
            var ix = this.listeners.indexOf(callback);
            this.listeners.splice(ix, 1);
        }

        type.prototype.notify = function (ix, kind, value) {
            if (this.listeners) {
                for (var ix = 0; ix < this.listeners.length;) {
                    var listener = this.listeners[ix];
                    try {
                        listener(ix, kind, value);
                        ix += 1;
                    } catch (e) { // Somehow the listener is inactive, remove
                        if (e instanceof ns.ListenerInvalidException) {
                            this.stopListening(listener);
                        }
                        else { throw e; }
                    }
                }
            }
        }

    }

    ns.Value = function (s) {
        var value = s;
        this.set = function (s) {
            value = s;
            this.notify();
        }
        this.get = function () {
            return value;
        }
        this.toString = function () {
            return value;
        }
    }
    ns.Observable(ns.Value);

    ns.Field = function (s) {
        var value = new ns.Value(s);
        return value;
    }


    ns.ListenArray = function (s) {

        this.set = function (ix, v) { this[ix] = v; this.notify(ix, "set", this); };
        this.get = function (ix) { return this[ix]; };
        this.push = function () { var r = Array.prototype.push.apply(this, arguments); this.notify(this.length - 1, "push", this); return r; };
        this.pop = function () { var r = Array.prototype.pop.apply(this, arguments); this.notify(this.length - 1, "pop", this); return r; };
        this.reverse = function () { var r = Array.prototype.reverse.apply(this, arguments); this.notify(-1, "reverse", this); return r; };
        this.shift = function () { var r = Array.prototype.shift.apply(this, arguments); this.notify(0, "shift", this); return r; };
        this.sort = function () { var r = Array.prototype.sort.apply(this, arguments); this.notify(-1, "sort", this); return r; };
        this.splice = function () { var r = Array.prototype.splice.apply(this, arguments); this.notify(arguments[0], "splice", this); return r; };
        this.unshift = function () { var r = Array.prototype.unshift.apply(this, arguments); this.notify(0, "unshift", this); return r; };
        this.toString = function () { return Array.prototype.toString.apply(this, arguments); };

        for (var i = 0, l = s.length; i < l; i++) {
            this.push(s[i]);
        }
    };
    ns.Observable(ns.ListenArray);


    ns.CallHelper = function (template, helper, target) {
        var args = Array.prototype.slice.apply(arguments);
        var helperArgs = args.slice(2);
        function call() {
            $(target).empty();
            if (typeof helper == 'string') {
                template[helper].apply(template, helperArgs);
            }
            else {
                helper.apply(template, helperArgs);
            }
        }
        return call;
    }

    ns.CallInContext = function (context, helper) {
        var args = Array.prototype.slice.apply(arguments);
        var helperArgs = args.slice(2);
        function call() {
            if (typeof helper == 'string') {
                context[helper].apply(context, helperArgs);
            }
            else {
                helper.apply(context, helperArgs);
            }
        }
        return call;
    }

    ns.Call = function (helper) {
        var args = Array.prototype.slice.apply(arguments);
        var helperArgs = args.slice(1);
        function call() {
            helper.apply(this, helperArgs);
        }
        return call;
    }

    ns.ListenerInvalidException = function () { }

    ns.Bind = function (observable, target, dontNotify) {
        if ($(target).is(":input")) {
            $(target).val(observable.get());
            if (!dontNotify) {
                observable.notify();
            }
            $(target).change(function () {
                observable.set($(this).val());
            });
        }
    }

    ns.BindListen = function (observable, target) {

        function onChange(node, observable) {
            if (!$(target).closest('html').length) {
                throw new CubeBuild.ListenerInvalidException();
            }
            if ($(target).is(":input")) {
                $(target).val(observable.get());
            }
            else {
                $(target).text(observable.get());
            }
        }

        onChange(target, observable);
        observable.listen(function () {
            onChange(target, observable);
        });
    }

    ns.BindSync = function (observable, target) {
        CubeBuild.BindListen(observable, target);
        CubeBuild.Bind(observable, target);
    }

    return ns;

    ns.JQBinding = {
        Value: function (sel) {
            this.selector = sel;
        },

        Attribute: function (sel, attr) {
            this.selector = sel;
            this.attribute = attr;
        },

        Self: function () {
        },

        SelfAttribute: function (attr) {
            this.attribute = attr;
        },

        ExplicitAttribute: function (sel, attr) {
            this.selector = sel;
            this.attribute = attr;
        },

        ExplicitValue: function (sel) {
            this.selector = sel;
        },

    };
})(CubeBuild);

var cb = cb || CubeBuild;


(function ($) {
    $.fn.template = function (template, model, empty) {
        empty = empty ? empty : true;
        $.each(this, function (nodeix, node) {
            var target = node;

            if (empty) { $(target).empty(); }
            var t = new template();
            var dialect = "string";
            if (t.$Dialect == "dom") {
                t.render(target, model);
            }
            if (!t.$Dialect || t.$Dialect == "string") {
                var result = t.render(model);
                target.insertAdjacentHTML('beforeend', result);
            }

            if (t.OnRender) {
                t.OnRender($(target));
            }
        });
    };

    $.fn.helper = function (template, helper, model, empty) {
        empty = empty ? empty : true;
        $.each(this, function (nodeix, node) {
            var target = node;

            if (empty) { $(target).empty(); }
            var t = new template();
            var dialect = "string";
            if (t.$Dialect == "dom") {
                t[helper](target, model);
            }
            if (!t.$Dialect || t.$Dialect == "string") {
                var result = t[helper](model);
                target.insertAdjacentHTML('beforeend', result);
            }

            if (t.OnRender) {
                t.OnRender($(target));
            }
        });
    };

    $.fn.bindObject = function (template, observable, empty) {
        function onChange(node, template, observable, empty) {
            if (!$(node).closest('html').length) {
                throw new CubeBuild.ListenerInvalidException();
            }
            $(node).template(template, observable, empty);
        }
        $.each(this, function (nodeix, node) {
            onChange(node, template, observable, empty);
            observable.listen(function () {
                onChange(node, template, observable, empty);
            });
        });

    }

    $.fn.postTemplate = function (url, data, template, rendered, empty) {
        empty = empty ? empty : true;
        $.each(this, function (nodeix, node) {
            var target = node;
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.Success) {
                        if (empty) {
                            $(target).empty();
                        }
                        var t = new template();
                        var dialect = "string"
                        if (t.$Dialect == "dom") {
                            t.render(target, data);
                        }
                        if (!t.$Dialect || t.$Dialect == "string") {
                            var result = t.render(data);
                            target.insertAdjacentHTML('beforeend', result);
                        }
                        if (t.OnRender) {
                            t.OnRender($(target), data);
                        }
                        if (rendered) {
                            rendered($(target), data);
                        }
                    }
                    else {
                        $(target).html(data.Message);
                    }
                }
            });
        });
    }

    $.postDialog = function (model, template, options) {
        var defaults = {
            buttons: {
                "OK": function () {
                    $(this).find("form").submit();
                    $(this).dialog('close');
                },
                "Cancel": function () {
                    $(this).dialog('close');
                }
            },
            close: function () {
                $(this).dialog('destroy');
                $(this).remove();
            }
        };

        var settings = $.extend(options, defaults);
        var t = new template();
        var d = $("<div/>");
        if (t.$Dialect == "string") {
            d.empty().append($(t.render(model)));
        }
        else if (t.$Dialect == "dom") {
            t.render(d[0], model);
        }

        dialog = d.dialog(settings);

        d.find("form").submit(function (e) {
            e.preventDefault();
            var form = $(this);
            var data = form.serialize();
            var url = form.attr("action");
            $.post(url, data, function (r) {
                if (settings.Result.Success) {
                    settings.Result.Success(r);
                }
            });
        });
    };

    $.fn.getAttribute = function (attribute, selector) {
        if (!selector) {
            selector = "[" + attribute + "]";
        }

        return $(this).parents(selector).first().attr(attribute);
    }

    $.fn.element = function (parentSelector, selector) {
        return $(this).parents(parentSelector).first().find(selector);
    }

    $.fn.bindTemplate = function (url, bindings, template, targetSelector, event) {

        // Checks the internal [[Class]] name of the object.
        function isFunction(object) {
            var getClass = {}.toString;
            return object && getClass.call(object) == '[object Function]';
        }

        function change(node) {
            data = {};
            $.each(bindings, function (bindingix, binding) {
                var val = binding;
                if (binding instanceof Binding.Value) {
                    val = $(node).find(binding.selector).val();
                }
                if (binding instanceof Binding.Attribute) {
                    val = $(node).find(binding.selector).attr(binding.attribute);
                }
                if (binding instanceof Binding.Self) {
                    val = $(node).val();
                }
                if (binding instanceof Binding.ExplicitAttribute) {
                    val = $(binding.selector).attr(binding.attribute);
                }
                if (binding instanceof Binding.ExplicitValue) {
                    val = $(binding.selector).val();
                }
                if (binding instanceof Binding.SelfAttribute) {
                    val = $(node).attr(binding.attribute);
                }
                data[bindingix] = val;
            });
            if (isFunction(url)) {
                $(targetSelector).template(template, url(data));
            }
            else {
                $(targetSelector).postTemplate(url, data, template);
            }
        }

        event = event ? event : "change";
        $.each(this, function (nodeix, node) {
            $.each(bindings, function (bindingix, binding) {
                if (event == change && binding instanceof Binding.Value) {
                    $(binding.selector, node).bind(event, function () {
                        change(node);
                    });
                }
                else {
                    $(node).bind(event, function () {
                        change(node);
                    });
                }
            });

            if (event == "change") {
                change(node);
            }
        });

    };

})(jQuery);
