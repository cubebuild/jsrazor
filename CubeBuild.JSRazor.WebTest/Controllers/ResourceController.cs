﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace CubeBuild.JSRazor.WebTest.Controllers
{
    public class ResourceController : Controller
    {
        public ActionResult JSTmpl(string viewController, string viewAction, string dialect = "string")
        {
            return this.JSTemplate(viewController, viewAction, dialect:dialect);
        }

        public ActionResult JSTmplFolder(string dialect = "dom")
        {
            var viewPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Fixed");
            return this.JSTemplateFolder(dialect, true, viewPath);
        }
    }
}
