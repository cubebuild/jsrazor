﻿$(function () {
    Render("Content", null, Content);
    Render("Code Block", null, CodeBlock);
    Render("Model Encoding", "<span style='color:green'>Passed!</span>", ModelEncoding);
    Render("For Loops", null, For);
    Render("Helper", null, Helper);
    Render("If", null, If);
    Render("Partial", null, Partial);
    Render("Attributes", null, Attributes);
    Render("Special", null, Special);
});

function Render(name, model, template) {
    var results = $("#testResults");

    var t = new template();
    var set = $("<fieldset/>");
    set.append("<legend>" + name + "</legend>")
    set.append(t.render(model));
    results.append(set);
}