﻿$(function () {
    Render("Content", null, Content);
    Render("Code Block", null, CodeBlock);
    Render("Model Encoding", "<span style='color:green'>Passed!</span>", ModelEncoding);
    Render("For Loops", null, For);
    Render("Helper", null, Helper);
    Render("If", null, If);
    Render("Partial", null, Partial);
    Render("Attributes", null, Attributes);
    Render("Event", null, Event);
    Render("Special", null, Special);
    Render("SVG", null, SVG);
    Render("Set", null, Set);
    Render("Null", null, Null);
});

function Render(name, model, template) {
    var results = $("#testResults");

    var t = new template();
    var set = $("<fieldset/>");
    set.append("<legend>" + name + "</legend>")
    t.render(set[0], model);
    results.append(set);
}