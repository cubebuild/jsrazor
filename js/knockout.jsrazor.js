﻿/*
Part of the CubeBuild.JSRazor project
Replaces Knockout templates with JSRazor precompiled templates
Licensed- GPL 3
*/

/// Keep the root context, template is attached to this
var root = this;

ko.jsrazorTemplateEngine = function () {
    this['allowTemplateRewriting'] = false;
}

/// Inherit from base template engine
ko.jsrazorTemplateEngine.prototype = new ko.templateEngine();

/// Render template using the template name
ko.jsrazorTemplateEngine.prototype['renderTemplateSource'] = function (templateSource, bindingContext, options) {

    var template = new root[options.name]();
    return ko.utils.parseHtmlFragment(template.render(bindingContext.$data));
};

/// We only care if the template cant be resolved. No source returned, we dont need it
ko.jsrazorTemplateEngine.prototype['makeTemplateSource'] = function (template, templateDocument) {
    if (!(root[template])) {
        throw new Error("JSRazor template with the name " + template + " is not available.");
    }
    return null;
}

/// Make Knockout use JSRazor pre-rendered templates
ko.jsrazorTemplateEngine.instance = new ko.jsrazorTemplateEngine();
ko.setTemplateEngine(ko.jsrazorTemplateEngine.instance);
