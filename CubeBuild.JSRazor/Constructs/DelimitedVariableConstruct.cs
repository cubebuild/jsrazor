﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Handle variable references that are complex, and therefore need to be delimited.
    /// Example @(Model.names["first"].toString())
    /// Content is rendered as escaped HTML
    /// </summary>
    public class DelimitedVariableConstruct:Construct
    {
        public DelimitedVariableConstruct()
            : base(Matcher.Chain(m => m.Literal("@").Surrounded('(', ')')))
        {
            
        }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var variable = Matcher.Values(input)[2];
            engine.Dialect.Encoded(builder, variable);
        }
    }
}
