﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Render a helper as a javascript function, interpreting the helper contents as content or code as appropriate
    /// Example @helper heading(value) { <h1>@value</h1> }
    /// </summary>
    class HelperConstruct: Construct
    {
        public HelperConstruct() :
            base(Matcher.Chain(m => m.Literal("@helper").Space().Regex("[a-zA-Z_.]*").Surrounded('(', ')').Space().Surrounded('{', '}'))) { }

        public override void RenderTo(ProcessingEngine engine, string input)
        {
            var values = Matcher.Values(input);
            var fn = values[3];
            var param = values[4];
            var inner = values[6];

            FunctionDef functionDef = new FunctionDef() { Name = fn, Params = param, Kind = FunctionDef.FunctionKind.helper };

            engine.RenderFunction(inner, functionDef);
        }

    }
}
