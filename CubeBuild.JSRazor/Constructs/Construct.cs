﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Abstract class defining the matching mechanism used to identify a construct, such as @if (condition) { block } else { block }
    /// It is also responsible for rendering the construct equivalent in javascript
    /// </summary>
    public abstract class Construct
    {
        protected Matcher Matcher {get; private set; }

        public Construct(Matcher matcher)
        {
            Matcher = matcher;
        }

        public virtual bool IsMatch(string input, out int length)
        {
            length = 0;
            return Matcher.IsMatch(input, ref length);
        }

        public virtual void Render(ProcessingEngine engine, string input, StringBuilder builder) { }

        public virtual void RenderTo(ProcessingEngine engine, string input)
        {
            
        }
    }
}
