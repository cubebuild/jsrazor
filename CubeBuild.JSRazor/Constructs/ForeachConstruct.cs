﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/GPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Render a standard javascript for loop, but using a foreach construct from Razor
    /// Example @foreach (var name in Model.Names) { @: Hi @name <br/> }
    /// In javascript this is rendered as a for iterator across an array with a unique counter variable.
    /// </summary>
    class ForeachConstruct: Construct
    {
        Matcher boundsMatcher = Matcher.Chain(m => m.Literal("var").Space().Regex("[a-zA-Z0-9_]+").Space().Literal("in").Space().Regex(".+"));
        public ForeachConstruct() :
            base(Matcher.Chain(m => m.Literal("@foreach").Space().Surrounded('(', ')').Space().Surrounded('{', '}'))) { }

        public override bool IsMatch(string input, out int length)
        {
            if (base.IsMatch(input, out length))
            {
                var values = Matcher.Values(input);
                var bounds = values[3];
                int boundsLength = 0;
                if (boundsMatcher.IsMatch(bounds, ref boundsLength))
                {
                    return true;
                }
            }
            return false;
        }
        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var values = Matcher.Values(input);
            var bounds = values[3];
            var inner = values[5];

            var boundsValues = boundsMatcher.Values(bounds);
            var variable = boundsValues[3];
            var inVariable = boundsValues[7];

            engine.Dialect.ForEach(builder, engine, variable, inVariable, inner);

        }

    }
}
