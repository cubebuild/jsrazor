﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Handle a call construct to call a helper or other javascript function
    /// Example @call(model)
    /// The returned value from the call is placed into the output result
    /// </summary>
    public class RenderConstruct:Construct
    {
        public RenderConstruct()
            : base(Matcher.Chain(m => m.Literal("@render").Space().Regex("[a-zA-Z0-9_]+").Surrounded('(', ')')))
        {
            
        }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var variables = Matcher.Values(input);
            var template = variables[3];
            var param = variables[4];
            engine.Dialect.Partial(builder, template, engine.Dialect.Main, param);
        }

    }
}
