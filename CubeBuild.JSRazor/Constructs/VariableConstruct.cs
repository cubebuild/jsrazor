﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Render a variable value, escaped
    /// example @name or @Model.Name.First
    /// A period at the end of the clause is interpreted and rendered as a period, not as part of the variable clause
    /// This can include array and dictionary references
    /// </summary>
    public class VariableConstruct:Construct
    {
        public VariableConstruct()
            : base(Matcher.Chain(m => m.Literal("@").Regex("[a-zA-Z0-9_.\\[\\]{}'\"]*[a-zA-Z0-9\\[\\]{}]{1}")))
        {
            
        }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var variable = Matcher.Values(input)[2];
            engine.Dialect.Encoded(builder, variable);
        }

    }
}
