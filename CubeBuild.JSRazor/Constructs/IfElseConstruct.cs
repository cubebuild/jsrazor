﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// A simple if ... else
    /// Example @if (true) { @: I am always true! } else { @: Oops, something is wrong... }
    /// The condition is a standard javascript condition clause
    /// </summary>
    class IfElseConstruct : Construct
    {
        public IfElseConstruct() :
            base(Matcher.Chain(m => m.Literal("@if").Space().Surrounded('(', ')').Space().Surrounded('{', '}').Space().Literal("else").Space().Surrounded('{', '}'))) { }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var values = Matcher.Values(input);
            var cond = values[3];
            var inner = values[5];
            var elseInner = values[9];
            engine.Dialect.IfElse(builder, engine, cond, inner, elseInner);
        }

    }
}
