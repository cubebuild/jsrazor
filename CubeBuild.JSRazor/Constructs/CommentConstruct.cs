﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Render a comment allowing it in source and target
    /// Example @* A Comment *@ will be rendered to the javascript as /* A Comment */
    /// </summary>
    public class CommentConstruct:Construct
    {
        public CommentConstruct()
            : base(Matcher.Chain(m => m.Regex("@[*].*?[*]@")))
        {
            
        }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var values = Matcher.Values(input);
            var comment = values[1];
            engine.Dialect.Comment(builder, comment.Substring(2, comment.Length - 4));
        }

    }
}
