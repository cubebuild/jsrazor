﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    public class SpecialConstruct:Construct
    {
        /// <summary>
        /// Write an unescaped variable value to the view
        /// Example @Html.Raw("<p>new</p>")
        /// This will be interpreted by the browser as html, rather than shown as text
        /// </summary>
        public SpecialConstruct()
            : base(Matcher.Chain(m => m.Literal("&").Regex(".*?;")))
        {
            
        }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var special = Matcher.Values(input)[2];
            engine.Dialect.Special(builder, "&" + special);
        }

    }
}
