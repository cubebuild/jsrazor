﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Render a helper as a javascript function, interpreting the helper contents as content or code as appropriate
    /// Example @helper heading(value) { <h1>@value</h1> }
    /// </summary>
    class EventConstruct: Construct
    {
        public EventConstruct() :
            base(Matcher.Chain(m => m.Literal("@event").Space().Regex("[a-zA-Z]*").Surrounded('(', ')'))) { }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var values = Matcher.Values(input);
            var evt = values[3];
            var fn = values[4];
            var target = "Target";

            engine.Dialect.Event(builder, target, evt, fn);
        }

    }
}
