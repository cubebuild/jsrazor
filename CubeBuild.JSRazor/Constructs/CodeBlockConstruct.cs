﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Define a code block, rendered verbatim to the javascript output
    /// Example @{ var i = 0; call(i); }
    /// </summary>
    public class CodeBlockConstruct : Construct
    {
        public CodeBlockConstruct() :
            base(Matcher.Chain(m => m.Literal("@").Surrounded('{', '}')))
        {
        }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var codeBlock = Matcher.Values(input)[2];
            engine.Dialect.CodeBlock(builder, codeBlock);
        }
    }
}
