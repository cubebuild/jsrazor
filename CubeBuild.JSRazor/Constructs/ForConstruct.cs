﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Render a for loop, using a standard javascript condition, and interpreting the contained content block
    /// Example @for (var i = 0; i < 10; i++) { <p>@i</p> }
    /// </summary>
    class ForConstruct: Construct
    {
        public ForConstruct() :
            base(Matcher.Chain(m => m.Literal("@for").Space().Surrounded('(', ')').Space().Surrounded('{', '}'))) { }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var values = Matcher.Values(input);
            var cond = values[3];
            var inner = values[5];
            engine.Dialect.For(builder, engine, cond, inner);
        }

    }
}
