﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Constructs
{
    /// <summary>
    /// Handle a call construct to call a helper or other javascript function
    /// Example @call(model)
    /// The returned value from the call is placed into the output result
    /// </summary>
    public class CallHelperConstruct:Construct
    {
        public CallHelperConstruct()
            : base(Matcher.Chain(m => m.Literal("@").Regex("[a-zA-Z0-9_.]+").Space().Surrounded('(', ')')))
        {
            
        }

        public override void Render(ProcessingEngine engine, string input, StringBuilder builder)
        {
            var variables = Matcher.Values(input);
            var helper = variables[2];
            var param = variables[4];
            engine.Dialect.CallHelper(builder, helper, param);
        }

    }
}
