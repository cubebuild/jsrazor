﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeBuild.JSRazor.Parser
{
    /// <summary>
    /// One function generated as a result of the parser
    /// </summary>
    public class FunctionDef
    {
        public FunctionDef() { 
            Body = new StringBuilder();
            Kind = FunctionKind.function;
        }

        public enum FunctionKind : int { function = 0, helper = 1 }
        public string Name { get; set; }
        public string Params { get; set; }
        public StringBuilder Body { get; set; }
        public FunctionKind Kind { get; set; }

        // Render the function to Javascript using a specific format
        public void RenderTo(string functionFormat, StringBuilder builder) { builder.AppendFormat(functionFormat, Name, Params, Body); }

    }
}
