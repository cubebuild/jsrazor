﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeBuild.JSRazor.Parser
{
    /// <summary>
    /// Match a section to a regular expression, or to whitespace characters
    /// </summary>
    public static class RegexExtension
    {
        public static Matcher Regex(this Matcher previous, string pattern)
        {
            RegexHandler regex = new RegexHandler(pattern);
            regex.SetPrevious(previous);
            return regex;
        }

        public static Matcher Space(this Matcher previous)
        {
            RegexHandler regex = new RegexHandler(@"\s*");
            regex.SetPrevious(previous);
            return regex;
        }

        private class RegexHandler:Matcher
        {
            string _pattern;
            public RegexHandler(string pattern)
            {
                _pattern = "^" + pattern;
            }

            public override bool IsMatch(string input, ref int length)
            {
                if (input == null) return false;

                var match = System.Text.RegularExpressions.Regex.Match(input, _pattern, System.Text.RegularExpressions.RegexOptions.Singleline);
                var next = input.Substring(match.Length);
                if (match.Success)
                {
                    length += match.Length;
                    return base.IsMatch(input.Substring(match.Length), ref length);
                }
                else return false;
            }

            public override string Value(string input, out string following)
            {
                var match = System.Text.RegularExpressions.Regex.Match(input, _pattern, System.Text.RegularExpressions.RegexOptions.Singleline);
                if (!match.Success)
                    throw new Exception("Match not found at matcher position");
                following = input.Substring(match.Length);
                return match.Value;
            }


        }
    }
}
