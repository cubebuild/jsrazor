﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeBuild.JSRazor.Parser
{
    /// <summary>
    /// Matcher is responsible for testing a clause at a specific position
    /// </summary>
    public class Matcher
    {
        /// <summary>
        /// Chain matching conditions together beneath an empty matcher
        /// </summary>
        /// <param name="m">chain of match extensions</param>
        /// <returns>the base matcher</returns>
        /// <example>Matcher.Chain(m => m.Literal("@"))</example>
        public static Matcher Chain(Func<Matcher, Matcher> m)
        {
            var baseMatcher = new Matcher();
            m(baseMatcher);

            return baseMatcher;
        }
        
        // The next matcher to consider
        public  Matcher Next { get; protected set; }

        // From a known previous match, set its next one to this one
        public void SetPrevious(Matcher previous) {
            previous.Next = this;
        }

        // Test if this match and all next matches are true
        public virtual bool IsMatch(string input, ref int length)
        {
            if (Next == null) return true;
            return Next.IsMatch(input, ref length);
        }

        // Derive the value for this matcher from an input string
        public virtual string Value(string input, out string following)
        {
            following = input;
            return "";
        }

        // Get the values of this and following matchers, returning the result as a string
        public List<string> Values(string input)
        {
            var result = new List<string>();
            var next = this;
            string following;
            while (next != null)
            {
                result.Add(next.Value(input, out following));
                input = following;
                next = next.Next;
            }

            return result;
        }

    }
}
