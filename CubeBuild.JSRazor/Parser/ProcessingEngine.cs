﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CubeBuild.JSRazor.Constructs;
using CubeBuild.JSRazor.Dialects;

namespace CubeBuild.JSRazor.Parser
{
    /// <summary>
    /// The enging that parses a template, it:
    /// Works from beginning to end
    /// Match a section against the defintions in the list of constructs, in added order
    /// Process a found construct or
    /// Render the found content as text if there was not match
    /// </summary>
    public class ProcessingEngine
    {
        // A list of helper functions found in the template
        List<FunctionDef> _functions = new List<FunctionDef>();

        BaseDialect _dialect;
        public BaseDialect Dialect { get { return _dialect; } }
        // Set up dialect
        public ProcessingEngine(BaseDialect dialect)
        {
            _dialect = dialect;
        }

        // Keep parameters but move to a different dialect
        public void SwitchDialect(BaseDialect dialect)
        {
            dialect.Model = _dialect.Model;
            dialect.Main = _dialect.Main;
            _dialect = dialect;
        }

        // Render from the root of a template, creating the standard render function to render the base template
        public string Render(string template, BaseDialect dialect)
        {

            _functions.Clear(); // Clear out previously rendered functions
            var functionDef = new FunctionDef() { 
                Name = _dialect.Main, 
                Params = _dialect.Model,
                Kind = FunctionDef.FunctionKind.helper
            };
            
            RenderFunction(template, functionDef, false);

            StringBuilder result = new StringBuilder();
            foreach (var fn in _functions)
            {
                Dialect.Function(result, fn);
            }

            return result.ToString();
        }

        // Render a new function, especially a helper, in addition to the base function render
        public void RenderFunction(string template, FunctionDef functionDef, bool insideBlock = true)
        {
            var result = functionDef.Body;
            Dialect.OpenContent(result);

            if (insideBlock)
                RenderCodeThenContent(template, result);
            else
                RenderContent(template, result);

            Dialect.CloseContent(result);
            RenderFunctionBody(functionDef);
        }

        // Render out an already valid javascript body
        public void RenderFunctionBody(FunctionDef function)
        {
            _functions.Add(function);
        }

        // Render a block section, treating it as code until we find @, @: or an html tag, then treating it as content
        public void RenderCodeThenContent(string template, StringBuilder result)
        {
            var blockMatcher = Matcher.Chain(m => m.Surrounded('{', '}'));
            for (var i = 0; i < template.Length; )
            {
                var segment = template.Substring(i);
                int length = 0;
                // When a code block is found,  even if a JS code block, recurse through it
                // This ensures an unmatched brace is found later in the template
                // It also means code processing is resumed after a block, even if the block contained a content reference
                if (blockMatcher.IsMatch(segment, ref length))
                {
                    var value = blockMatcher.Values(segment);
                    Dialect.StartBlock(result);
                    RenderCodeThenContent(value[1], result);
                    Dialect.EndBlock(result);

                    // Content block is handled, skip beyond it 
                    i += length;
                    continue;
                }
                else
                {
                    var match = System.Text.RegularExpressions.Regex.Match(segment, "^(@:|@|<[a-zA-Z].*?[/]?>)");
                    if (match.Success)
                    {
                        if (segment.StartsWith("@@"))
                        {
                            result.Append("@");
                            i += 2;
                        }
                        else
                        {
                            result.Append("\n");
                            if (match.Value == "@:")
                                RenderContent(segment.Substring(match.Index + 2), result);
                            else
                                RenderContent(segment.Substring(match.Index), result);

                            // Content rendering handles the remainder of the block, get out
                            break;
                        }
                    }
                    else if (i < template.Length)
                    {
                        result.Append(template[i]);
                        i++;
                    }
                }
            }
        }

        // Process the template construct by construct, writing non-construct content as text, 
        public int RenderContent(string template, StringBuilder result, string stopAt = null)
        {
            Regex stopAtRegex = null;
            if (!String.IsNullOrEmpty(stopAt))
                stopAtRegex = new Regex(stopAt, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            var buffer = new StringBuilder();
            int ix;
            for (ix = 0; ix < template.Length; )
            {
                // Write each newline in the template onto a new code line in the rendered JS
                // NOTE: newline is not written into the final HTML result ATM
                if (template[ix] == '\n')
                {
                    if (buffer.Length != 0)
                    {
                        Dialect.Literal(result, buffer.ToString());
                        buffer.Clear();
                    }
                    ix++;
                    continue;
                }


                var segment = template.Substring(ix);
                if (stopAtRegex != null)
                {
                    var stopMatch = stopAtRegex.Match(segment);
                    if (stopMatch.Success)
                    {
                        ix += stopMatch.Length;
                        break;
                    }
                }

                var found = false;
                // Look for a construct at the current position, and prcess it if one is found
                // Then skip the construct to the next segment
                // NOTE: Constructs should use a literal match as their first clause where possible so the construct is abandoned quickly
                foreach (var construct in Dialect.Constructs)
                {
                    int length;
                    if (construct.IsMatch(segment, out length))
                    {
                        if (buffer.Length != 0)
                        {
                            Dialect.Literal(result, buffer.ToString());
                            buffer.Clear();
                        }
                        construct.Render(this, segment, result);
                        construct.RenderTo(this, segment);
                        found = true;
                        ix += length;
                        break;
                    }
                }

                if (segment.StartsWith("<"))
                {
                    if (buffer.Length != 0)
                    {
                        Dialect.Literal(result, buffer.ToString());
                        buffer.Clear();
                    }

                    var match = Regex.Match(segment, @"^<([a-zA-Z0-9]+)\s{0,1}([^>]*?)/>");
                    if (match.Success) // start and closed tag
                    {
                        var tag = match.Groups[1].Value;
                        var attributes = match.Groups[2].Value;
                        Dialect.ClosedTag(result, this, tag, attributes);
                        ix += match.Length;
                        found = true;
                    }
                    else
                    {
                        match = Regex.Match(segment, @"^<([a-zA-Z0-9]+)\s{0,1}(.*?)(?<!/)>", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        if (match.Success) // start tag
                        {
                            var tag = match.Groups[1].Value;
                            var attributes = match.Groups[2].Value;


                            var stopAtPattern = String.Format(@"^</\s*{0}\s*>", tag, RegexOptions.Singleline | RegexOptions.IgnoreCase);

                            Dialect.OpenTag(result, this, tag, attributes);

                            var inner = segment.Substring(match.Length);
                            
                            int length = RenderContent(inner, result, stopAtPattern);

                            Dialect.CloseTag(result, this, tag);

                            ix += match.Length + length;
                            found = true;
                        }
                    }


                }

                // If no construct was found, treat it as content and just append it
                if (!found)
                {
                    buffer.Append(template[ix]);
                    ix++;
                }
            }

            // Still some left? append it
            if (buffer.Length != 0)
                Dialect.Literal(result, buffer.ToString());

            return ix;
        }
    }
}
