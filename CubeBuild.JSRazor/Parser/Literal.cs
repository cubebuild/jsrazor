﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeBuild.JSRazor.Parser
{

    /// <summary>
    /// Parse and match a literal value
    /// </summary>
    public static class LiteralExtension
    {
        public static Matcher Literal(this Matcher previous, string match){
            LiteralHandler literal = new LiteralHandler(match);
            literal.SetPrevious(previous);
            return literal;
        }

        private class LiteralHandler : Matcher
        {
            string _match;
            public LiteralHandler(string match)
            {
                _match = match;
            }

            public override bool IsMatch(string input, ref int length)
            {
                if (input == null) return false;

                if (input.StartsWith(_match))
                {
                    length += _match.Length;
                    return base.IsMatch(input.Substring(_match.Length), ref length);
                }
                else
                    return false;
            }

            public override string Value(string input, out string following)
            {
                if (!input.StartsWith(_match))
                    throw new Exception("Parsing Exception, literal not at extraction position");

                following = input.Substring(_match.Length);
                return _match;
            }
        }
    }
}
