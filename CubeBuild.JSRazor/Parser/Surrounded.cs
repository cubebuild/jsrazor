﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeBuild.JSRazor.Parser
{
    /// <summary>
    /// Match a section surrounded by two characters
    /// In this string, * marks the matched characters when Surrounded('(', ')') is used
    /// *( ( ) ( ( ) ) )*
    /// The entire interal content is matched and the value excludes the surrounding characters.
    /// </summary>
    public static class SurroundedExtension
    {
        public static Matcher Surrounded(this Matcher previous, char start, char end)
        {
            SurroundedHandler surrounded = new SurroundedHandler(start, end);
            surrounded.SetPrevious(previous);
            return surrounded;
        }

        private class SurroundedHandler : Matcher
        {
            char _start, _end;

            public SurroundedHandler(char start, char end)
            {
                _start = start;
                _end = end;
            }

            public override bool IsMatch(string input, ref int length)
            {
                if (String.IsNullOrEmpty(input) || input[0] != _start)
                    return false;

                string following;

                var matched = (string)null;
                matched = Value(input, out following, false);
                if (input.StartsWith(matched))
                {
                    length += matched.Length;
                    return base.IsMatch(following, ref length);
                }
                else
                    return false;
            }

            public override string Value(string input, out string following)
            {
                return Value(input, out following, true);
            }

            public string Value(string input, out string following, bool trimSurrounding = true)
            {
                bool sq = false, dq=false;
                int rp=0;
                int ix = 0;
                int co = 1;
                while (++ix < input.Length && co > 0)
                {
                    sq ^= !dq && input[ix] == '\'';
                    dq ^= !sq && input[ix] == '"';
                    if (!sq && !dq)
                    {
                        if (input[ix] == _start)
                            co++;

                        if (input[ix] == _end)
                            co--;

                        rp = ix;
                    }
                }

                if (sq || dq)
                    throw new Exception(String.Format("Runaway quote found at postion {0} of {1}",rp, input)); 

                if (co > 0)
                    throw new Exception(String.Format("Unmatched {0} and {1} parsing near {2}.", _start, _end, input));

                following = input.Substring(ix);
                if (trimSurrounding)
                    return input.Substring(1, ix -2);
                else
                    return input.Substring(0, ix);
            }
        }
    }
}
