﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Constructs;
using CubeBuild.JSRazor.Dialects;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor
{
    /// <summary>
    /// Setup an engine with the known constructs
    /// </summary>
    public class RazorToJSRenderingEngine
    {

        // Set up an engine with valid constructs
        public ProcessingEngine CreateEngine( BaseDialect dialect)
        {
         var _engine = new ProcessingEngine(dialect ?? new StringDialect());
            return _engine;
        }

        // Transform a string template using a specific template name
        public string Transform(string template, string templateName, BaseDialect dialect = null)
        {
            var _engine = CreateEngine(dialect);

            var normalTemplate = template.Replace("\r\n", "\n");
            var content = _engine.Render(normalTemplate, dialect);

            StringBuilder objectCode = new StringBuilder();
            _engine.Dialect.Container(objectCode, templateName, content);
            return objectCode.ToString();
        }


        // Transform from a text reader, using a template name
        public string Transform(TextReader reader, string templateName, BaseDialect dialect = null)
        {
            return Transform(reader.ReadToEnd(), templateName, dialect);
        }
    }
}
