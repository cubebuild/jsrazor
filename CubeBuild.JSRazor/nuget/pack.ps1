
function build
{
  write "++++++++++++++++++++++++++++++++++++++++++++++++++" | out-default
  write "++  BUILDING CUBEBUILD CORE                     ++" | out-default
  write "++++++++++++++++++++++++++++++++++++++++++++++++++" | out-default
  pushd ..\..
  c:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe /p:Configuration=Release /t:Rebuild CubeBuild.JSRazor.sln | out-default
  popd
  
  return $LASTEXITCODE
}

function exec_nuget {
  write "++++++++++++++++++++++++++++++++++++++++++++++++++" | out-default
  write "++  PACKAGING NUGET                             ++" | out-default
  write "++++++++++++++++++++++++++++++++++++++++++++++++++" | out-default
  ls *.ps1 -ex pack.ps1 | foreach {& $_.FullName}

  write "++++++++++++++++++++++++++++++++++++++++++++++++++" | out-default
  write "++  DONE                                        ++" | out-default
  write "++++++++++++++++++++++++++++++++++++++++++++++++++" | out-default
}

$result = build;

if ($result -eq 0) {
  exec_nuget
}
