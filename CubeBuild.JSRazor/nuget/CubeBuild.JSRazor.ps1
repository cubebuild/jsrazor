pushd CubeBuild.JSRazor

rm lib -recurse
ni lib/net40 -itemtype d | out-null

cp ..\..\bin\release\CubeBuild.JSRazor.dll lib\net40\
cp ..\..\..\CubeBuild.JSRazor.Web.MVC\bin\release\CubeBuild.JSRazor.Web.MVC.dll lib\net40\

$p = rvpa lib\net40\CubeBuild.JSRazor.dll
$v = get-childitem $p | select-object -expandproperty versioninfo | select-object -expandproperty ProductVersion

nuget pack -version $v.ToString()
mv *.nupkg .. -force

popd
