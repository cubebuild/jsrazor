﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CubeBuild.JSRazor.Parser;

namespace CubeBuild.JSRazor.Dialects
{
    public class StringDialect : BaseDialect
    {

        protected string FunctionFormat = "this.{0} = function({1}) {{\n{2}\n}};\n";

        public override void Function(StringBuilder result, FunctionDef function)
        {
            result.AppendFormat(FunctionFormat, function.Name, function.Params, function.Body.ToString());
        }

        public override void OpenContent(StringBuilder result)
        {
            result.Append("rslt = \"\";\n");
        }

        public override void CloseContent(StringBuilder result)
        {
            result.Append("\nreturn rslt;\n");
        }

        protected string LiteralFormat = "rslt += \"{0}\";\n";

        public override void Literal(StringBuilder result, string literal)
        {
            result.AppendFormat(LiteralFormat, literal.Replace("\"", "\\\""));
        }

        public override void OpenTag(StringBuilder result, ProcessingEngine engine, string tag, string attributes)
        {
            if (String.IsNullOrWhiteSpace(attributes))
                result.Append(String.Format("rslt += \"<{0}>\";\n", tag));
            else
            {
                StringBuilder attributeResult = new StringBuilder();
                engine.RenderContent(attributes, attributeResult);
                result.Append(String.Format("rslt += \"<{0} \";\n{1}\nrslt += \">\";\n", tag, attributeResult));
            }

        }

        public override void CloseTag(StringBuilder result, ProcessingEngine engine, string tag)
        {
            result.Append(String.Format("rslt += \"</{0}>\";\n", tag));
        }

        public override void ClosedTag(StringBuilder result, ProcessingEngine engine, string tag, string attributes)
        {
            if (String.IsNullOrWhiteSpace(attributes))
                result.Append(String.Format("rslt += \"<{0}/>\";\n", tag));
            else
            {
                StringBuilder attributeResult = new StringBuilder();
                engine.RenderContent(attributes, attributeResult);
                result.Append(String.Format("rslt += \"<{0} \";\n{1}\nrslt += \"/>\";\n", tag, attributeResult));
            }

        }

        public override void StartBlock(StringBuilder result)
        {
            result.Append("\n{\n");
        }

        public override void EndBlock(StringBuilder result)
        {
            result.Append("\n}\n");
        }

        protected string CallHelperFormat = "rslt += {0}({1});\n";

        public override void CallHelper(StringBuilder result, string helper, string parameters)
        {
            result.AppendFormat(CallHelperFormat, helper, parameters);

        }

        public override void CodeBlock(StringBuilder result, string codeBlock)
        {
            result.Append(codeBlock.Trim());
        }
        public override void Comment(StringBuilder result, string comment)
        {
            result.Append("/*").Append(comment).Append("*/");
        }
        
        protected string EncodedFormat = "rslt += (({0})?({0}):\"\").toString().replace(/&/g,\"&amp;\").replace(/</g, \"&lt;\").replace(/>/g, \"&gt;\");\n";

        public override void Encoded(StringBuilder result, string encoded)
        {
            result.AppendFormat(EncodedFormat, encoded.Trim());
        }

        protected string RawFormat = "rslt += {0};\n";

        public override void Raw(StringBuilder result, string raw)
        {
            result.AppendFormat(RawFormat, raw);
        }

        protected string SpecialFormat = "rslt += \"{0}\";\n";
        public override void Special(StringBuilder result, string special)
        {
            result.AppendFormat(SpecialFormat, special);
        }

        protected string PartialFormat = "rslt += new {0}().{1}({2});\n";
        public override void Partial(StringBuilder result, string className, string methodName, string parameters)
        {
            result.AppendFormat(PartialFormat, className, methodName, parameters);
        }
    }
}