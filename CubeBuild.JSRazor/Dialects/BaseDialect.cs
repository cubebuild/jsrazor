﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/
using System;
using System.Collections.Generic;
using System.Text;
using CubeBuild.JSRazor.Constructs;
using CubeBuild.JSRazor.Parser;
namespace CubeBuild.JSRazor.Dialects
{
    public abstract class BaseDialect
    {
        public BaseDialect()
        {
            Main = "render";
            Model = "Model";
            Constructs.Add(new CommentConstruct());
            Constructs.Add(new SpecialConstruct());
            Constructs.Add(new HtmlRawConstruct());
            Constructs.Add(new SetConstruct());
            Constructs.Add(new ForeachConstruct());
            Constructs.Add(new ForConstruct());
            Constructs.Add(new IfElseConstruct());
            Constructs.Add(new IfConstruct());
            Constructs.Add(new HelperConstruct());
            Constructs.Add(new RenderHelperConstruct());
            Constructs.Add(new RenderConstruct());
            Constructs.Add(new FunctionConstruct());
            Constructs.Add(new CodeBlockConstruct());
            Constructs.Add(new CallHelperConstruct());
            Constructs.Add(new VariableConstruct());
            Constructs.Add(new DelimitedVariableConstruct());
        }

        private readonly List<Construct> _constructs = new List<Construct>();
        public List<Construct> Constructs { get { return _constructs; } }

        public string Main { get; set; }
        public string Model { get; set; }
        private int variableName = 1;
        protected string VariableName() { return String.Format("_{0}", (variableName++).ToString("X")); }

        protected static string ContainerFormat = "function {0}() {{\n{1}\nthis.$Dialect = \"{2}\";\n}}";
        public void Container(StringBuilder result, string templateName, string content)
        {
            string dialect = "undefined";
            if (this is StringDialect) dialect = "string";
            if (this is DOMDialect) dialect = "dom";

            result.AppendFormat(ContainerFormat, templateName, content, dialect);
        }

        public abstract void Function(StringBuilder result, FunctionDef def);

        public abstract void OpenContent(StringBuilder result);
        public abstract void CloseContent(StringBuilder result);
        public abstract void Literal(StringBuilder result, string literal);
        public abstract void OpenTag(StringBuilder result, ProcessingEngine engine, string tag, string attributes);
        public abstract void CloseTag(StringBuilder result, ProcessingEngine engine, string tag);
        public abstract void ClosedTag(StringBuilder result, ProcessingEngine engine, string tag, string attributes);
        public abstract void StartBlock(StringBuilder result);
        public abstract void EndBlock(StringBuilder result);
        public abstract void CallHelper(StringBuilder result, string helper, string parameters);
        public abstract void CodeBlock(StringBuilder result, string codeBlock);
        public abstract void Comment(StringBuilder result, string comment);
        public abstract void Encoded(StringBuilder result, string encoded);
        public abstract void Raw(StringBuilder result, string raw);
        public abstract void Special(StringBuilder result, string special);
        public virtual void For(StringBuilder result, ProcessingEngine engine, string condition, string inner)
        {
            result.AppendFormat("for ({0}) {{\n", condition);
            engine.RenderCodeThenContent(inner, result);
            result.Append("\n}\n");
        }

        public virtual void ForEach(StringBuilder result, ProcessingEngine engine, string variable, string inVariable, string inner)
        {
            var variableName = VariableName();

            result.AppendFormat("for (var {2} = 0; {2} < {1}.length; {2}++) {{\n var {0} = {1}[{2}];\n", variable, inVariable, variableName);
            engine.RenderCodeThenContent(inner, result);
            result.Append("\n}\n");

        }

        public virtual void If(StringBuilder result, ProcessingEngine engine, string condition, string inner)
        {
            result.AppendFormat("if ({0}) {{\n", condition);
            engine.RenderCodeThenContent(inner, result);
            result.Append("\n}\n");
        }

        public virtual void IfElse(StringBuilder result, ProcessingEngine engine, string condition, string inner, string elseInner)
        {
            result.AppendFormat("if ({0}) {{\n", condition);
            engine.RenderCodeThenContent(inner, result);
            result.Append("\n} else {\n");
            engine.RenderCodeThenContent(elseInner, result);
            result.Append("\n}\n");
        }
        public abstract void Partial(StringBuilder result, string className, string methodName, string parameters);

        public virtual void Event(StringBuilder result, string target, string evt, string fn)
        {
            throw new NotSupportedException("The event fragment is not supported in this dialect");
        }


        public virtual void Set(StringBuilder result, string attr, string param)
        {
            throw new NotSupportedException("The set fragment is not supported in this dialect.");
        }
    }
}
