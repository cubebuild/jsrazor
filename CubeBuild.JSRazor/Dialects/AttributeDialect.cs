﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubeBuild.JSRazor.Constructs;

namespace CubeBuild.JSRazor.Dialects
{
    /// <summary>
    /// A minimal dialect that supports rendering of variables and raw values only.
    /// This is switched to by other dialects, and is generally not used directly
    /// </summary>
    public class AttributeDialect:StringDialect
    {
        public AttributeDialect()
        {
            Main = "render";
            Model = "Model";
            Constructs.Clear();
            Constructs.Add(new HtmlRawConstruct());
            Constructs.Add(new VariableConstruct());
            Constructs.Add(new DelimitedVariableConstruct());
        }
    }
}
