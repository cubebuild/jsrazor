﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/LGPL-3.0
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using CubeBuild.JSRazor.Parser;
using CubeBuild.JSRazor.Constructs;

namespace CubeBuild.JSRazor.Dialects
{
    public class DOMDialect : BaseDialect
    {

        public DOMDialect()
        {
            Main = "render";
            Model = "Model";
            Constructs.Insert(10, new EventConstruct());
            Constructs.Insert(11, new AttachConstruct());
        }


        protected string FunctionFormat = "this.{0} = function({1}) {{\n{2}\n}};\n";
        protected string HelperFormat = "this.{0} = function({1}) {{\n{2}\n}};\n";
        public Stack<string> namespaces = new Stack<string>();

        public override void Function(StringBuilder result, FunctionDef function)
        {
            var parameters = function.Params;
            if (function.Kind == FunctionDef.FunctionKind.helper)
            {
                parameters = String.IsNullOrWhiteSpace(parameters) ? "Target" : "Target, " + parameters;
                result.AppendFormat(HelperFormat, function.Name, parameters, function.Body.ToString());
            }
            else 
                result.AppendFormat(FunctionFormat, function.Name, parameters, function.Body.ToString());
        }

        public override void OpenContent(StringBuilder result)
        {
        }

        public override void CloseContent(StringBuilder result)
        {
        }

        protected string LiteralFormat = "var {0} = document.createTextNode(\"{1}\");\nTarget.appendChild({0});\n";

        public override void Literal(StringBuilder result, string literal)
        {
            if (!String.IsNullOrEmpty(literal))
                result.AppendFormat(LiteralFormat, VariableName(), literal.Replace("\"", "\\\""));
        }

        public override void OpenTag(StringBuilder result, ProcessingEngine engine, string tag, string attributes)
        {
            var variableName = VariableName();
            if (attributes.Contains("xmlns"))
            {
                var nsMatch = Regex.Match(attributes, "xmlns=[\"']{0,1}(.*?)[\"']{0,1}[ >]");
                if (nsMatch.Success)
                {
                    var ns = nsMatch.Groups[1].Value;
                    namespaces.Push(ns);
                }
            }
            else
            {
                if (namespaces.Count == 0)
                    namespaces.Push("");
                else
                    namespaces.Push(namespaces.Peek());
            }

            if (String.IsNullOrEmpty(namespaces.Peek())) {
                result.Append(String.Format("var {0} = document.createElement(\"{1}\");\n", variableName, tag));
            }
            else{
                result.Append(String.Format("var {0} = document.createElementNS(\"{1}\", \"{2}\");\n", variableName, namespaces.Peek(), tag));
            }
            result.Append(String.Format("Target.appendChild({0});\n", variableName));

            if (!String.IsNullOrWhiteSpace(attributes)) {
                ProcessAttributes(result, engine, attributes, variableName);
            }

            result.AppendFormat("var $Context = this.Prev = {0};\n(function(Target){{\n", variableName);

        }

        public override void CloseTag(StringBuilder result, ProcessingEngine engine, string tag)
        {
            result.Append(String.Format("}}).call(this, $Context);\nthis.Prev = $Context;\n"));
            namespaces.Pop();
        }

        public override void ClosedTag(StringBuilder result, ProcessingEngine engine, string tag, string attributes)
        {
            var variableName = VariableName();
            if (attributes.Contains("xmlns"))
            {
                var nsMatch = Regex.Match(attributes, "xmlns=[\"']{0,1}(.*?)[\"']{0,1}[ >]");
                if (nsMatch.Success)
                {
                    var ns = nsMatch.Groups[1].Value;
                    namespaces.Push(ns);

                    attributes = Regex.Replace(attributes, "xmlns=[\"']{0,1}.*?[\"']{0,1}[ >]", "");
                }
            }
            else
            {
                if (namespaces.Count == 0)
                    namespaces.Push("");
                else
                    namespaces.Push(namespaces.Peek());
            }

            if (String.IsNullOrEmpty(namespaces.Peek())) {
                result.Append(String.Format("var {0} = document.createElement(\"{1}\");\n", variableName, tag));
            }
            else{
                result.Append(String.Format("var {0} = document.createElementNS(\"{1}\", \"{2}\");\n", variableName, namespaces.Peek(), tag));
            }
            result.Append(String.Format("Target.appendChild({0});\n", variableName));

            if (!String.IsNullOrWhiteSpace(attributes)) {
                ProcessAttributes(result, engine, attributes, variableName);
            }

            result.Append(String.Format("this.Prev = {0};\n", variableName));

            namespaces.Pop();

        }

        private static void ProcessAttributes(StringBuilder result, ProcessingEngine engine, string attributes, string variableName)
        {
            var dialect = engine.Dialect;
            engine.SwitchDialect(new AttributeDialect());
            for (int ix = 0; ix < attributes.Length; )
            {
                var segment = attributes.Substring(ix);
                var match = System.Text.RegularExpressions.Regex.Match(segment, @"^(([^\s]*?)\s*?=\s*?)(.*)$", RegexOptions.Singleline);
                if (match.Success)
                {
                    ix += match.Groups[1].Length; ; // skip beyond the attribute name
                    var attr = match.Groups[2].Value.Trim();
                    var val = match.Groups[3].Value.Trim();
                    var stopAt = @"^\s";
                    if (val.StartsWith("'")) { stopAt = @"^'"; val = val.Substring(1); ix += 1; }
                    if (val.StartsWith("\"")) { stopAt = @"^"""; val = val.Substring(1); ix += 1; }
                    StringBuilder attributeResult = new StringBuilder();
                    var length = engine.RenderContent(val, attributeResult, stopAt);
                    result.AppendFormat("rslt = \"\";\n{0}", attributeResult.ToString());
                    result.AppendFormat("{0}.setAttribute(\"{1}\", rslt);\n", variableName, attr);
                    ix += length;
                }
                else ix++;
            }
            engine.SwitchDialect(dialect);
        }

        public override void StartBlock(StringBuilder result)
        {
            result.Append("\n{\n");
        }

        public override void EndBlock(StringBuilder result)
        {
            result.Append("\n}\n");
        }

        protected string CallHelperFormat = "{0}({1});\n";

        public override void CallHelper(StringBuilder result, string helper, string parameters)
        {
            if (String.IsNullOrWhiteSpace(parameters)) parameters = "Target";
            else parameters = "Target," + parameters;
            result.AppendFormat(CallHelperFormat, helper, parameters);

        }

        public override void CodeBlock(StringBuilder result, string codeBlock)
        {
            result.Append(codeBlock.Trim());
        }

        public override void Comment(StringBuilder result, string comment)
        {
            result.Append("/*").Append(comment).Append("*/");
        }

        protected string EncodedFormat = "Target.insertAdjacentHTML('beforeend', (({0}!==null&&typeof({0})!=='undefined')?({0}):\"\").toString().replace(/&/g,\"&amp;\").replace(/</g, \"&lt;\").replace(/>/g, \"&gt;\"));\n";

        public override void Encoded(StringBuilder result, string encoded)
        {
            if (!String.IsNullOrEmpty(encoded))
                result.AppendFormat(EncodedFormat, encoded);
        }

        protected string RawFormat = "Target.insertAdjacentHTML('beforeend', ({0}).toString());\n";

        public override void Raw(StringBuilder result, string raw)
        {
            if (!String.IsNullOrEmpty(raw))
                result.AppendFormat(RawFormat, raw);
        }

        protected string SpecialFormat = "Target.insertAdjacentHTML('beforeend', \"{0}\");\n";

        public override void Special(StringBuilder result, string special)
        {
            result.AppendFormat(SpecialFormat, special);
        }

        protected string PartialFormat = "new {0}().{1}({2});\n";
        public override void Partial(StringBuilder result, string className, string methodName, string parameters)
        {
            if (String.IsNullOrWhiteSpace(parameters)) parameters = "Target";
            else parameters = "Target, " + parameters;
            result.AppendFormat(PartialFormat, className, methodName, parameters);
        }

        public override void Event(StringBuilder result, string target, string evt, string fn)
        {
            result.AppendFormat("{0}.addEventListener('{1}', {2}, false);\n", target, evt, fn);
        }

        public override void Set(StringBuilder result, string attr, string param)
        {
            result.AppendFormat("this.Prev.setAttribute('{0}', {1});\n", attr, param);
        }
    }
}