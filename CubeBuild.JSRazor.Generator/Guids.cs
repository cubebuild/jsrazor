﻿/***************************************************************************

Copyright (c) Microsoft Corporation. All rights reserved.
This code is licensed under the Visual Studio SDK license terms.
THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

***************************************************************************/

using System;

namespace CubeBuld.JSRazor.Generator
{
    static class GuidList
    {
        public const string guidSimpleFileGeneratorString = "20A8D5A6-CA39-40F0-9A89-CF26EE48A9E8";
        public static readonly Guid guidSimpleFileGenerator = new Guid(guidSimpleFileGeneratorString);
    };
}