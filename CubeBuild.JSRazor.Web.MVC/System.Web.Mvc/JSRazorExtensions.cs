﻿/*
CubeBuild.JSRazor is distributed under the GNU Public License.
http://opensource.org/licenses/GPL-3.0
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CubeBuild.JSRazor;
using CubeBuild.JSRazor.Dialects;
using Microsoft.Ajax.Utilities;

namespace System.Web.Mvc
{
    public static class RazorClientTemplateExtensions
    {

        /// <summary>
        /// Given a controller, and action, render all its templates to a single JS file for download
        /// </summary>
        /// <param name="callingController">the controller which called us</param>
        /// <param name="controller">the controller to load views for</param>
        /// <param name="action">the action to load views for</param>
        /// <param name="assemblyName">the assembly, if any, where views are embedded</param>
        /// <returns>a contentresult containing the refered javascript</returns>
        /// <remarks>
        /// Views should be stored in the conventional folders, below a folder with the same name as the view, so for an action called Home/Index, views would be from files *.jshtml
        /// > /Views/Home/Index/
        ///                     one.jshtml
        ///                     two.jshtml
        /// </remarks>
        public static ActionResult JSTemplate(this Controller callingController, string controller, string action, string assemblyName = null, string dialect = "string", bool considerMinify = true)
        {
            var Engine = new RazorToJSRenderingEngine();
            BaseDialect transformDialect;
            switch (dialect)
            {
                case "dom": transformDialect = new DOMDialect(); break;
                case "string":
                default: transformDialect = new StringDialect(); break;
            }

            var minify = false;
            if (considerMinify)
            {
                minify = !IsAssemblyDebugBuild(callingController.GetType().Assembly);
            }

            var templates = GetViewTemplates(controller, action, assemblyName);
            StringBuilder templateResult = new StringBuilder();
            foreach (var template in templates)
            {
                if (template is TemplateBody)
                {
                    var templateBody = template as TemplateBody;
                    templateResult.Append(Engine.Transform(templateBody.GetContent(), templateBody.Name, transformDialect));
                } else
                if (template is CodeBody)
                {
                    templateResult.Append(template.GetContent()).Append("\n\n");
                }
            }
            if (minify)
            {
                Minifier m = new Minifier();
                var minified = m.MinifyJavaScript(templateResult.ToString());
                return new ContentResult() { Content = minified, ContentType = "text/javascript" };
            }
            else
                return new ContentResult() { Content = templateResult.ToString(), ContentType = "text/javascript" };

        }


        /// <summary>
        /// Given a path(s) probe for javascript and templates
        /// </summary>
        /// <param name="callingController">the controller which called us</param>
        /// <param name="paths">Paths to search through</param>
        /// <returns>a contentresult containing the refered javascript</returns>
        /// <remarks>
        /// Any number of folders can be passed, all will be used
        /// </remarks>
        public static ActionResult JSTemplateFolder(this Controller callingController, string dialect = "string", bool considerMinify = true, params string[] paths)
        {
            var Engine = new RazorToJSRenderingEngine();
            BaseDialect transformDialect;
            switch (dialect)
            {
                case "dom": transformDialect = new DOMDialect(); break;
                case "string":
                default: transformDialect = new StringDialect(); break;
            }

            var minify = false;
            if (considerMinify)
            {
                minify = !IsAssemblyDebugBuild(callingController.GetType().Assembly);
            }

            var templates = new List<CodeBody>();
            foreach (var path in paths.Where(p => Directory.Exists(p)))
            {
                ProbeJS(templates, path);
                ProbeViews(templates,path);
            }

            StringBuilder templateResult = new StringBuilder();
            foreach (var template in templates)
            {
                if (template is TemplateBody)
                {
                    var templateBody = template as TemplateBody;
                    templateResult.Append(Engine.Transform(templateBody.GetContent(), templateBody.Name, transformDialect));
                }
                else
                    if (template is CodeBody)
                    {
                        templateResult.Append(template.GetContent()).Append("\n\n");
                    }
            }
            if (minify)
            {
                Minifier m = new Minifier();
                var minified = m.MinifyJavaScript(templateResult.ToString());
                return new ContentResult() { Content = minified, ContentType = "text/javascript" };
            }
            else
                return new ContentResult() { Content = templateResult.ToString(), ContentType = "text/javascript" };

        }

        /// <summary>
        /// Determine if the calling assembly is built in debug mode
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        private static bool IsAssemblyDebugBuild(Assembly assembly)
        {
            foreach (var attribute in assembly.GetCustomAttributes(false))
            {
                var debuggableAttribute = attribute as DebuggableAttribute;

                if (debuggableAttribute != null)
                {
                    return debuggableAttribute.IsJITTrackingEnabled;
                }
            }
            return false;
        }

        /// <summary>
        /// Interface for appendable content
        /// </summary>
        public class CodeBody
        {
            public string Body { get; set; }

            public virtual string GetContent() { return this.Body; }
        }

        /// <summary>
        /// A template reference, where Name is [Name].jshtml
        /// </summary>
        public class TemplateBody : CodeBody
        {
            public string Name { get; set; }
        }


        /// <summary>
        /// Probe physical folders or find embedded resources in the right location
        /// </summary>
        /// <param name="controller">the controller</param>
        /// <param name="action">the action</param>
        /// <param name="assemblyName">the assembly if any</param>
        /// <returns>a list of view templates to render</returns>
        private static IEnumerable<CodeBody> GetViewTemplates(string controller, string action, string assemblyName)
        {
            var templates = new List<CodeBody>();
            if (String.IsNullOrEmpty(assemblyName)) // physical views
            {
                // Probe for views in the action folder
                var viewPath = String.Format("Views/{0}/{1}/", controller, action);
                ProbeJS(templates, viewPath);
                ProbeViews(templates, viewPath);

                // Probe for views in /views/controller folder
                viewPath = String.Format("Views/{0}/", controller);
                ProbeJS(templates, viewPath);
                ProbeViews(templates, viewPath);

                // Probe for views in /views/shared
                viewPath = String.Format("Views/Shared/");
                ProbeJS(templates, viewPath);
                ProbeViews(templates, viewPath);
            }
            else // embedded resources
            {
                // Probe for views in the action folder
                string viewExpression = String.Format("^.*[.]Views[.]{0}[.]{1}[.][^.]*[.]jshtml$", controller, action);
                string jsExpression = String.Format("^.*[.]Views[.]{0}[.]{1}[.][^.]*[.]js$", controller, action);
                var assembly = Assembly.Load(assemblyName);
                ProbeEmbeddedJS(templates, jsExpression, assembly);
                ProbeEmbeddedViews(templates, viewExpression, assembly);

                // Probe for views in the controller folder
                viewExpression = String.Format("^.*[.]Views[.]{0}[.][^.]*[.]jshtml$", controller);
                jsExpression = String.Format("^.*[.]Views[.]{0}[.][^.]*[.]js$", controller);
                ProbeEmbeddedJS(templates, jsExpression, assembly);
                ProbeEmbeddedViews(templates, viewExpression, assembly);

                // Probe for jshtml views in /views/shared/
                viewExpression = @"^.*[.]Views[.]Shared[.][^.]*[.]jshtml$";
                jsExpression = @"^.*[.]Views[.]Shared[.][^.]*[.]js$";
                ProbeEmbeddedJS(templates, jsExpression, assembly);
                ProbeEmbeddedViews(templates, viewExpression, assembly);
            }
            return templates;
        }


        private static void ProbeEmbeddedViews(List<CodeBody> templates, string viewExpression, Assembly assembly)
        {
            var viewNames = assembly.GetManifestResourceNames().Where(n => System.Text.RegularExpressions.Regex.IsMatch(n, viewExpression, Text.RegularExpressions.RegexOptions.IgnoreCase));
            foreach (var name in viewNames)
            {
                using (var stream = assembly.GetManifestResourceStream(name))
                using (var reader = new StreamReader(stream))
                {
                    var match = System.Text.RegularExpressions.Regex.Match(name, "([a-zA-Z0-9]+)[.]jshtml$", Text.RegularExpressions.RegexOptions.IgnoreCase);
                    templates.Add(new TemplateBody() { Body = reader.ReadToEnd(), Name = match.Groups[1].Value });
                }
            }
        }

        private static void ProbeEmbeddedJS(List<CodeBody> templates, string viewExpression, Assembly assembly)
        {
            var viewNames = assembly.GetManifestResourceNames().Where(n => System.Text.RegularExpressions.Regex.IsMatch(n, viewExpression, Text.RegularExpressions.RegexOptions.IgnoreCase));
            foreach (var name in viewNames)
            {
                using (var stream = assembly.GetManifestResourceStream(name))
                using (var reader = new StreamReader(stream))
                {
                    templates.Add(new CodeBody() { Body = reader.ReadToEnd() });
                }
            }
        }

        private static void ProbeViews(List<CodeBody> templates, string viewPath)
        {
            var basePath = Hosting.HostingEnvironment.ApplicationPhysicalPath;
            var searchPath = Path.Combine(basePath, viewPath);
            if (Directory.Exists(searchPath))
            {
                var files = Directory.GetFiles(searchPath, "*.JSHTML");
                foreach (var file in files)
                {
                    using (var stream = new StreamReader(file))
                    {
                        var match = System.Text.RegularExpressions.Regex.Match(file, "([a-zA-Z0-9_-]+)[.]jshtml$", Text.RegularExpressions.RegexOptions.IgnoreCase);
                        templates.Add(new TemplateBody() { Body = stream.ReadToEnd(), Name = match.Groups[1].Value });
                    }
                }
            }
        }

        private static void ProbeJS(List<CodeBody> templates, string viewPath)
        {
            var basePath = Hosting.HostingEnvironment.ApplicationPhysicalPath;
            var searchPath = Path.Combine(basePath, viewPath);
            if (Directory.Exists(searchPath))
            {
                var files = Directory.GetFiles(searchPath, "*.JS");
                foreach (var file in files)
                {
                    using (var stream = new StreamReader(file))
                    {
                        templates.Add(new CodeBody() { Body = stream.ReadToEnd() });
                    }
                }
            }
        }
    }
}
